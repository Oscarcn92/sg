import numpy as np
from matplotlib import pyplot as plt


UUID = "d41be869-824d-48a5-b8e7-0154c204a678"

f = open("build-linux-x86_64/newview/packaged/image"+UUID+".txt", "r")
line = f.readline()
Data = []
rgb = [0,0,0]
i = 0
while  line != "DATA END!!!!!!!!\n":
    rgb[i] = int(line[:-1])  
    i += 1
    if i == 3:
        Data.append(rgb[:])
        i = 0
    line = f.readline()
# while  line != "DATA END!!!!!!!!\n":
#     Data.append(line[:-1])
#     line = f.readline()
width = int(f.readline()[:-1])
height = int(f.readline()[:-1])
f.readline()
imgType = int(f.readline()[:-1])
print(width, height, imgType)

dataNew = []

for i in range(0, width):
    dataNew.append(Data[height*i:height*(i+1)])

npData = np.array(dataNew)

print (npData.size)

# image = Image.fromarray(npData, "RGB")
# image.show()

# image.save("TestResult.png")

plt.imshow(npData, interpolation='nearest')
plt.show()