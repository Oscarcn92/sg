# -*- cmake -*-

include(Variables)
include(FreeType)

set(API_INCLUDE_DIRS
    ${LIBS_OPEN_DIR}/newview/rendering_engine/API
    )

if (BUILD_HEADLESS)
  set(API_HEADLESS_LIBRARIES
    APIheadless
      )
endif (BUILD_HEADLESS)
set(API_LIBRARIES
    API
    )

