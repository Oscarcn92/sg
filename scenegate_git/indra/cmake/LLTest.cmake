# -*- cmake -*-

include(Variables)
include(FreeType)

set(LLTEST_INCLUDE_DIRS
    ${LIBS_OPEN_DIR}/newview/rendering_engine/lltest
    )

if (BUILD_HEADLESS)
  set(LLRENDER_HEADLESS_LIBRARIES
      lltestheadless
      )
endif (BUILD_HEADLESS)
set(LLTEST_LIBRARIES
    lltest
    )

