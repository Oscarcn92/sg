/*************************************************************************/
/*  test_render.cpp                                                      */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           GODOT ENGINE                                */
/*                      https://godotengine.org                          */
/*************************************************************************/
/* Copyright (c) 2007-2021 Juan Linietsky, Ariel Manzur.                 */
/* Copyright (c) 2014-2021 Godot Engine contributors (cf. AUTHORS.md).   */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "test_render.h"
#include "main/performance.h"

#include "core/math/math_funcs.h"
#include "core/math/quick_hull.h"
#include "core/os/keyboard.h"
#include "core/os/main_loop.h"
#include "core/os/os.h"
#include "core/print_string.h"
#include "servers/visual_server.h"
#include "scene/resources/mesh.h"
#include "core/array.h"
#include "core/image.h"
#include <bits/stdc++.h>



#include <string>
#include <iostream>
#include <fstream>


using namespace std;
#define OBJECT_COUNT 1

//#define PATH "/home/svwd/Documents/Gauthier/SG/scenegate_git/build-linux-x86_64/newview/packaged/"
//#define PATH "/home/intern/Documents/Scenegate/BITBUCKET/sg/scenegate_git/build-linux-x86_64/newview/packaged/"

namespace TestRender {

class TestMainLoop : public MainLoop {


	// string PATH = "/home/intern/Documents/Scenegate/BITBUCKET/sg/scenegate_git/build-linux-x86_64/newview/packaged/";
	string PATH = "/home/svwd/Documents/Gauthier/SG/scenegate_git/build-linux-x86_64/newview/packaged/Meshes/";
	// string PATH =  "/home/svwd/Desktop/MeshesData/";
	// string PATH = "/home/intern/Documents/Gauthier/sg/scenegate_git/build-linux-x86_64/newview/packaged/Meshes/";
	string PATH2 = "/home/intern/Documents/Gauthier/sg/scenegate_git/build-linux-x86_64/newview/packaged/";
	// string PATH2 = "/home/svwd/Documents/Gauthier/SG/scenegate_git/build-linux-x86_64/newview/packaged/";
	
	std::list<std::string> listUUID ={};
	std::list<std::string> listUUIDDouble ={};
	std::set<std::string> listUUIDZero ={};


	RID cube;
	RID cubeInstance;
	Transform cubeTrans;
	bool renderUpdate = true;

	RID cube2;
	RID cubeInstance2;
	Transform cubeTrans2;

	RID texture;
	RID material;
	RID Object;
	RID object2;

	RID avatar;
	RID avatarInstance;


	Transform cube_t;
	Transform t;
	Transform c;
	Transform o;
	RID instance;
	RID camera;
	RID viewport;
	RID light;
	RID sky;
	RID scenario;
	int tempo =0;
	bool stop = false;
	int objNumber=0;
	Transform cameraT;

	AABB a ;
	Vector3 positionParent;
	struct InstanceInfo {

		RID instance;
		Transform base;
		std::string name;
	};
	InstanceInfo ii;
	List<InstanceInfo> instances;
	bool parentSet=false;

	float ofs_x, ofs_y;;
	bool quit;
	Performance *perfs = Performance::get_singleton();

	VisualServer *vs = VisualServer::get_singleton();

protected:
public:
	virtual void input_event(const Ref<InputEvent> &p_event) {
		
		 
		if (p_event->is_pressed()){
			// float r= t.get_basis()[0]
			
			// quit = true;
			print_line(p_event->as_text());
			stop=true;
		}
		else{
			return;
		}
		//DEBUG INFOS :
		if (p_event->as_text()=="A"){
		
			std::vector<String> RenderInfos ={

				"INFO_OBJECTS_IN_FRAME",
				"INFO_VERTICES_IN_FRAME",
				"INFO_MATERIAL_CHANGES_IN_FRAME",
				"INFO_SHADER_CHANGES_IN_FRAME",
				"INFO_SURFACE_CHANGES_IN_FRAME",
				"INFO_DRAW_CALLS_IN_FRAME",
				"INFO_2D_ITEMS_IN_FRAME",
				"INFO_2D_DRAW_CALLS_IN_FRAME",
				"INFO_USAGE_VIDEO_MEM_TOTAL",
				"INFO_VIDEO_MEM_USED",
				"INFO_TEXTURE_MEM_USED",
				"INFO_VERTEX_MEM_USED",
			};
			for(int i = 0;i !=12;i++){
			// OS::get_singleton()->print(perfs->get_monitor_name(Performance::Monitor(i))+"%f\n", perfs->get_monitor(Performance::Monitor(i)));
			print_line(RenderInfos[i]+":"+String::num_int64(vs->get_render_info(VisualServer::RenderInfo(i))));
			// print_line("ok");
			}
			print_line("FPS :"+String::num_real(perfs->get_monitor(Performance::Monitor::TIME_FPS)));
			// OS::get_singleton()->print(vs->is_render_loop_enabled());

		}
		//update or not 
		if (p_event->as_text()=="U"){
		
			renderUpdate= !renderUpdate;
		}

		
		//ROTA Lumiere
		if (p_event->as_text()=="D"){
		
			t.rotate(Vector3(0, 1, 0), 0.1);
			vs->instance_set_transform(light, t);
		}

		if (p_event->as_text()=="Q"){
		
			t.rotate_basis(Vector3(0, 1, 0), -0.1);
			vs->instance_set_transform(light, t);
		}

		if (p_event->as_text()=="Z"){
		
			t.rotate_basis(Vector3(1, 0, 0), 0.1);
			vs->instance_set_transform(light, t);
		}

		if (p_event->as_text()=="S"){
		
			t.rotate_basis(Vector3(1, 0, 0), -0.1);
			vs->instance_set_transform(light, t);
		}

		//rota cam
		if (p_event->as_text()=="I"){
			
			print_line(cameraT.operator String());
			
		}

		if (p_event->as_text()=="P"){
		
			cameraT.set_origin(Vector3(112,28,-66));
			// cameraT.set_origin(Vector3(116.175804, 29.519739, -26.835373));
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="F"){
		
			cameraT.rotate_basis(Vector3(0, 1, 0), 0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="H"){
		
			cameraT.rotate_basis(Vector3(0, 1, 0), -0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="T"){
		
			cameraT.rotate_basis(Vector3(1, 0, 0), 0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="G"){
		
			cameraT.rotate_basis(Vector3(1, 0, 0), -0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="R"){
		
			cameraT.rotate_basis(Vector3(0, 0, 1), 0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="Y"){
		
			cameraT.rotate_basis(Vector3(0, 0, 1), -0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		

		// if (p_event->as_text()=="P"){
		
		// 	// for(int i =0;i<instances.size();i++){
		// 	// 	instances[i].base.translate(Vector3(-150.894897,-69.506149,-27.294022));
		// 	// 	vs->instance_set_transform(instances[i].instance, instances[i].base);
		// 	// }
		// 	// instances[instances.size()-1].base.translate(positionParent);
		// 	// // instances[instances.size()-1].base.scale(Vector3(0.2,0.2,0.2));
		// 	// vs->instance_set_transform(instances[instances.size()-1].instance, instances[instances.size()-1].base);
		// 	// cameraT.translate(positionParent);
		// 	// vs->camera_set_transform(camera, cameraT);

		// 	cubeTrans.translate(Vector3(1,0,0));
		// 	cubeTrans2.translate(Vector3(-1,0,0));

		// 	vs->instance_set_transform(cubeInstance, cubeTrans);
		// 	vs->instance_set_transform(cubeInstance2, cubeTrans2);


			
		// }

		if (p_event->as_text()=="O"){
			
			cubeTrans2.scale(Vector3(2,2,2));

			vs->instance_set_transform(cubeInstance2, cubeTrans2);
		}

		if (p_event->as_text()=="W"){
			
			cubeTrans.scale(Vector3(2,2,2));
			
			vs->instance_set_transform(cubeInstance, cubeTrans);
		}

		if (p_event->as_text()=="X"){
			cubeTrans.scale(Vector3(0.5,0.5,0.5));
			vs->instance_set_transform(cubeInstance, cubeTrans);
		}

		if (p_event->as_text()=="C"){
			
			cubeTrans.scale_basis(Vector3(2,2,2));
			
			vs->instance_set_transform(cubeInstance, cubeTrans);
		}

		if (p_event->as_text()=="V"){
			cubeTrans.scale_basis(Vector3(0.5,0.5,0.5));
			vs->instance_set_transform(cubeInstance, cubeTrans);
		}

		
//DEPLACEMENTS CAMERA

		if (p_event->as_text()=="Up"){
		
			cameraT.translate(Vector3(0, 1, 0));
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="Down"){
		
			cameraT.translate(Vector3(0,-1, 0));
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="Left"){
		
			cameraT.translate(Vector3(-1, 0, 0));
			vs->camera_set_transform(camera, cameraT);
			
			// for(int i =0;i<instances.size();i++){
			// 	instances[i].base.translate(Vector3(-0.1, 0, 0));
			// 	vs->instance_set_transform(instances[i].instance, instances[i].base);
			// }
		}

		if (p_event->as_text()=="Right"){
		
			cameraT.translate(Vector3(1, 0, 0));
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="N"){
		
			cameraT.translate(Vector3(0, 0, 0.1));
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="Comma"){
		
			cameraT.translate(Vector3(0, 0, -0.1));
			
			vs->camera_set_transform(camera, cameraT);
		}

		///DEPLACEMENTS 
		if (p_event->as_text()=="Control+Up"){
		
			o.translate(Vector3(0, 0.1, 0));
			vs->instance_set_transform(ii.instance, o);
		}

		if (p_event->as_text()=="Control+Down"){
		
			o.translate(Vector3(0, -0.1, 0));
			vs->instance_set_transform(ii.instance, o);
		}

		if (p_event->as_text()=="Control+Left"){
		
			o.translate(Vector3(-0.1, 0, 0));
			vs->instance_set_transform(ii.instance, o);
		}

		if (p_event->as_text()=="Control+Right"){
		
			o.translate(Vector3(0.1, 0, 0));
			vs->instance_set_transform(ii.instance, o);
		}

		if (p_event->as_text()=="Control+Z"){
		
			o.translate(Vector3(0, 0, -0.1));
			vs->instance_set_transform(ii.instance, o);
		}

		if (p_event->as_text()=="Control+S"){
		
			o.translate(Vector3(0, 0, 0.1));
			vs->instance_set_transform(ii.instance, o);
		}
		////////////

		if (p_event->as_text()=="K"){
		
			o.scale(Vector3(1.1, 1.1, 1.1));
			vs->instance_set_transform(ii.instance, o);
		}

		if (p_event->as_text()=="L"){
		
			o.scale(Vector3(0.9, 0.9, 0.9));
			vs->instance_set_transform(ii.instance, o);
		}

		// print_line(cubeTrans.origin);
			
	}

	virtual void init() {

		print_line("INITIALIZING TEST RENDER");
		// VisualServer *vs = VisualServer::get_singleton();
		scenario = vs->scenario_create();

		// c.translate(Vector3(0, 2, 8));

		/* LIGHT */
		RID lightaux = vs->directional_light_create();

		
		
		vs->light_set_shadow(lightaux, true);
		light = vs->instance_create2(lightaux, scenario);
		
		// vs->light_set_bake_mode(lightaux, VisualServer::LightBakeMode::LIGHT_BAKE_INDIRECT);
		// vs->light_set_param(lightaux, VisualServer::LightParam::LIGHT_PARAM_INDIRECT_ENERGY, 0.5) ;
		Transform lla;
		lla.translate(Vector3(0, 10000, 0));
		// lla.rotate_basis(Vector3(0, 1, 0),0.25*Math_PI);
		// lla.rotate_basis(Vector3(0, 1, 0),Math_PI);
		lla.rotate_basis(Vector3(1, 0, 0),-0.5*Math_PI);
		// lla.translate(0, 0, 5);
		vs->instance_set_transform(light, lla);
		// vs->light_set_color(lightaux, Color(0.7, 0.7, 1.0));



		// RID lightaux2 = vs->directional_light_create();
		// vs->light_set_shadow(lightaux2, false);
		// RID light2 = vs->instance_create2(lightaux2, scenario);
		// Transform lla2;
		// lla2.translate(Vector3(124, 300, -70));
		// lla2.rotate_basis(Vector3(1, 0, 0),0.5*Math_PI);
		// // lla.translate(0, 0, 5);
		// vs->instance_set_transform(light2, lla2);

		// RID lightaux3 = vs->directional_light_create();
		// vs->light_set_shadow(lightaux3, false);
		// RID light3 = vs->instance_create2(lightaux3, scenario);
		// Transform lla3;

		// lla3.rotate_basis(Vector3(1, 0, 0),-0.5*Math_PI);
		// // lla.translate(0, 0, 5);
		// vs->instance_set_transform(light3, lla3);

		
		
		
		//lla.set_look_at(Vector3(),Vector3(1,-1,1),Vector3(0,1,0));
		// lla.set_look_at(Vector3(1,1,1), Vector3(-0.000000, -0.0, -0), Vector3(0, 0.1, 0));

		// vs->instance_set_transform(light, lla);

		/* CAMERA */

		camera = vs->camera_create();

		RID viewport = vs->viewport_create();
		Size2i screen_size = OS::get_singleton()->get_window_size();
		vs->viewport_set_size(viewport, screen_size.x, screen_size.y);
		vs->viewport_attach_to_screen(viewport, Rect2(Vector2(), screen_size));
		vs->viewport_set_active(viewport, true);
		vs->viewport_attach_camera(viewport, camera);
		vs->viewport_set_scenario(viewport, scenario);
		vs->viewport_set_render_direct_to_screen(viewport, true);
		
		
		// vs->camera_set_transform(camera, Transform(Basis(0.991685,-0.128690,0.000000  ,0.000000,0.000000,1.000000 , -0.128690,-0.991685,0.000000), Vector3( 105.296600,24.840773,73.526154)));
		
		
		// vs->camera_set_perspective(camera, 60, 0.1, 40.0);
		cameraT.translate(Vector3(0, 0, 10));
		// cameraT.rotate_basis(Vector3(0, 1, 0),Math_PI);
		vs->camera_set_transform(camera, cameraT);
		

		//ENVIRONMENT 

		RID env = vs->environment_create ( );
		
		// vs->environment_set_adjustment(env, true, 10, 10, 10);
		vs->environment_set_bg_color(env, Color(0.7, 0.7, 1.0));
		vs->environment_set_bg_energy (  env, 1 );
		vs->environment_set_ambient_light(env, Color(0.7, 0.7, 1.0), 0.1);
		
		vs->scenario_set_environment(scenario,env );


		// Transform cameratr;
		// cameratr.rotate(Vector3(0, 1, 0), 1);
		// cameratr.rotate(Vector3(1, 0, 0), 0);
		// cameratr.translate(Vector3(0, 2, 8));
		// // VisualServer *vs = VisualServer::get_singleton();
		// vs->camera_set_transform(camera, cameratr);

		// createSGObject();
		createSGObject2();
		createAvatar();
		// createGodotCube1();
		// createGodotCube2();

		ofs_x = ofs_y = 0;
		quit = false;
	}
	virtual bool iteration(float p_time) {
		
		
		// if(tempo%10 ==0 &&renderUpdate){
		// // if(false){
		// 	// print_line(String::num_int64(tempo));
			
			
		// 	std::fstream output_file;

		// 	bool test=false;
			
		// 	std::string a;
		// 	output_file.open(PATH2+"readAndWrite.txt", std::ios_base::in);
		// 	while (getline(output_file, a)){
		// 		// print_line("GG :");
		// 		// print_line("a :"+String(a.c_str()));
		// 		if(a=="ENDWRITE"){
		// 			// print_line("ok");
		// 			test=true;
		// 		}
		// 	}
		// 	output_file.close();
			
		// 	if(test){
				
				
		// 		output_file.open(PATH2+"readAndWrite.txt", std::ios_base::out |std::ios_base::trunc);
		// 		output_file << "ISREADING";
		// 		output_file.close();
				
		// 		print_line(String::num_int64(tempo));

		// 		updateAvatar();
				
		// 		output_file.open(PATH2+"readAndWrite.txt", std::ios_base::out |std::ios_base::trunc);
		// 		output_file << "ENDREADING";
		// 		output_file.close();
		// 	}
		// }
		// tempo++;




		
		// Transform cameratr;
		// cameratr.rotate(Vector3(0, 1, 0), ofs_x);
		// cameratr.rotate(Vector3(1, 0, 0), -ofs_y);
		// cameratr.translate(Vector3(0, 2, 8));
		// VisualServer *vs = VisualServer::get_singleton();
		// vs->camera_set_transform(camera, cameratr);

		// if((tempo%100 ==0)&&(!stop)){
		// 	t.rotate(Vector3(1, 1, 0).normalized(), 0.6);
		// 	vs->instance_set_transform(light, t);
		
		// }
		// tempo++;
		// if(stop){
		// 	stop=false;
		// 	VisualServer *vs = VisualServer::get_singleton();
		// 	t.rotate(Vector3(0, 1, 0), 0.1);
		// 	vs->instance_set_transform(light, t);
		
		// }
		




		// ofs_x += p_time * 0.35;

		//return quit;
		
		// for (List<InstanceInfo>::Element *E = instances.front(); E; E = E->next()) {
			
		// 	Transform pre(Basis(E->get().rot_axis, ofs_x), Vector3().normalized());
			
		// 	vs->instance_set_transform(E->get().instance, pre * E->get().base);
		// 	/*
		// 	if( !E->next() ) {
		// 		vs->free( E->get().instance );
		// 		instances.erase(E );
		// 	}*/
		// }

		
		






		return quit;
	}

	virtual bool idle(float p_time) {
		return quit;
	}

	virtual void finish() {
	}


	

	void createSGObject()
	{
		
		// VisualServer *vs = VisualServer::get_singleton();
		// Object = vs->mesh_create();
		// object2 = vs->mesh_create();
		
		// std::fstream myfile("/home/svwd/Documents/Gauthier/SG/scenegate_git/indra/newview/rendering_engine/godot-3.3/main/tests/ObjectsToRender", std::ios_base::in);
		std::fstream myfile(PATH+"objListUUID.txt", std::ios_base::in);

		
		std::string a;

		while (getline(myfile, a)){
			// if(objNumber%10==0){
			// 	createMesh(a, true);
			// }
			// objNumber++;
			if(a!="2531d2cf-8bcf-4208-b22f-d383f4b3514e"){
				RID object = vs->mesh_create();
				RID instance=vs->instance_create2(object, scenario);
				// createMesh(a, true);
				createMesh(a, object, instance, true);
			}
						
		}

		// createMesh("dadd6911-39b9-4db4-abe7-998faca383f0");
		// createMesh("00eee12a-ec77-4835-b01f-4d551ed0f512");
		// createGodotCube3WithTexture("dadd6911-39b9-4db4-abe7-998faca383f0");
		// createMesh("9bcced65-9630-4ee5-9bae-4907a6e530fb");

		// createMesh("a6d19614-1f20-4a3a-b401-b33ac31824ab",&object2, 6);

		// ii.instance = vs->instance_create2(Object, scenario);
		// instances.push_back(ii);

		// RID instance2 = vs->instance_create2(object2, scenario);
		// Transform t ;

		// t.translate(Vector3(-2, 0, -0));
		// t.rotate_basis(Vector3(0, 1, 0), -0.25 * Math_PI);
		
		
		// vs->instance_set_transform(instance2, t);
		// ii.base.rotate(Vector3(0, 1, 0), 1 * Math_PI);
		// ii.base.rotate(Vector3(1, 0, 0), 1 * Math_PI);
	}

//Les sous objects sont des objs et pas just des nouvells faces du meme object
	void createSGObject2()
	{
		
		// std::fstream myfile("/home/svwd/Documents/Gauthier/SG/scenegate_git/indra/newview/rendering_engine/godot-3.3/main/tests/ObjectsToRender", std::ios_base::in);
		std::fstream myfile(PATH+"objListUUID.txt", std::ios_base::in);

		
		std::string a;

		while (getline(myfile, a)){
			// if(objNumber%10==0){
			// 	createMesh(a, true);
			// }
			// objNumber++;
			
			// createMesh(a, true);
			// print_line("Call create MEsh");
			if(a!="2531d2cf-8bcf-4208-b22f-d383f4b3514e"){
				createMesh2(a, true);			
			}
		}
		print_line("List UUID  SIze:"+String::num_real(listUUID.size()));
		print_line("List UUID Double SIze:"+String::num_real(listUUIDDouble.size()));
		print_line("List UUID ZERO SIze:"+String::num_real(listUUIDZero.size()));
		
		// std::set<std::string>::iterator itr;
		// for (itr = listUUIDZero.begin();
		// 	itr != listUUIDZero.end(); itr++)
		// {
		// 	cout << *itr << "\n";
		// }
		

	}

	RID createTextureFromImg(String FileName) {
		Ref<Image> img = memnew(Image);
		img->load(FileName);
		RID imgTexture = vs->texture_create_from_image(img);
		// imgTexture->create_from_image(img);
		return imgTexture;
	}

	Ref<ImageTexture> createTextureFromImg2(String FileName) {
		Ref<Image> img = memnew(Image);
		Ref<ImageTexture> imgTexture = memnew(ImageTexture);
		img->load(FileName);
		//RID imgTexture = vs->texture_create_from_image(img);
		imgTexture->create_from_image(img);
		return imgTexture;
	}

	RID createTexture(std::string UUID){
		int width;
		int height;
		int dataSize;
		Image::Format format;

		PoolVector<uint8_t> Data = PoolByteArray();
		fillData(&Data, &dataSize, &width, &height, &format, UUID);

		Image *img = memnew(Image);
		img->create(width, height, 0, format, Data);
		RID imgTexture = vs->texture_create_from_image(img);
		return imgTexture;
	}

	void fillData(PoolVector<uint8_t> *Data, int *dataSize,int *width, int *height, Image::Format *format, std::string UUID) {
		
		std::ifstream myfile0(PATH+"image"+UUID+".txt");
		int etape = 0;
		std::string a;
		while (getline(myfile0, a)){
			if ((etape == 0) && (a != "DATA END!!!!!!!!")) 
				{ Data->append(std::stoi(a)); }
			else if (etape == 0) { etape = 1; }
			else if (etape == 1) { *width = std::stoi(a); etape = 2; }
			else if (etape == 2) { *height = std::stoi(a); etape = 3; }
			else if (etape == 3) { etape = 4; }
			else if (etape == 4) { 
				if (std::stoi(a) == 3) {*format = Image::FORMAT_RGB8;}
				if (std::stoi(a) == 4) {*format = Image::FORMAT_RGBA8;}
				etape = 5;
				}
			else if (etape == 5) { *dataSize = std::stoi(a); }
		}
	}


	// void createMesh(std::string UUID, bool isParent){
	void createMesh(std::string UUID, RID object, RID instance, bool first=false){

		Vector3 scale;
		Vector3 position;
		Vector3 rotation;
		float angle;
		int nbreFaces;
		std::vector<std::string> childrenUUID;
		
		fillHeaders(&scale,  &position, &rotation, &angle, &nbreFaces, &childrenUUID, UUID);
		
		// OS::get_singleton()->print("UUID : %s\n", UUID.c_str());
		// RID object = vs->mesh_create();
		// RID instance=vs->instance_create2(object, scenario);
		// RID texture = createTexture(UUID);
		// RID material = vs->material_create();
		// vs->material_set_param(material, "albedo_texture", texture);
		// Ref<SpatialMaterial> mat = memnew(SpatialMaterial);
		// mat->set_flag(SpatialMaterial::FLAG_ALBEDO_FROM_VERTEX_COLOR, true);
		// mat.instance();
		// mat->set_albedo(Color(0.7, 0.7, 1.0, 0.2));
		// mat->set_flag(SpatialMaterial::FLAG_UNSHADED, true);
		// mat->set_feature(SpatialMaterial::FEATURE_TRANSPARENT, true);
		// mat->set_albedo(Color(1.0,0.0,0.0));

		for(unsigned  a =0;a<nbreFaces;a++){
				
			// OS::get_singleton()->print("face %i\n", i);	

			PoolVector<Vector3> verticles= * memnew(PoolVector<Vector3>);
			PoolVector<Vector3> normals=* memnew(PoolVector<Vector3>);
			PoolVector<int> index=* memnew(PoolVector<int>);
			PoolVector<Color> color=* memnew(PoolVector<Color>);
			PoolVector<Vector2> uv =* memnew(PoolVector<Vector2>);
			
			vector<real_t> listOfPos = * memnew(vector<real_t>);
			vector<real_t> listOfNorm=* memnew(vector<real_t>);
			vector<real_t> listOfIndex=* memnew(vector<real_t>);
			
			fillVectors(&listOfPos, &listOfNorm, &listOfIndex, UUID+std::to_string(a));
			
			
			// Vector<Vector3> verticles;
			// Vector<Vector3> normals;
			// Vector<int> index;
			//We copy the values from the sg text files
			// print_line("aaaaaaaaaaa");
			
			for (unsigned i =0;i<=(listOfPos.size()-3);i+=3){
				
				
				verticles.push_back(Vector3(listOfPos[i],listOfPos[i+1],listOfPos[i+2])/*.normalized()*/);
				normals.push_back(Vector3(listOfNorm[i],listOfNorm[i+1],listOfNorm[i+2])/*.normalized()*/);
				color.push_back(Color(1, 0, 0));
				uv.push_back(Vector2(0, 0));
			} 

			
			for(unsigned  j =0;j<(listOfIndex.size());j+=3){
				index.push_back(listOfIndex[j+2]);
				index.push_back(listOfIndex[j+1]);
				index.push_back(listOfIndex[j]);

				// index.push_back(listOfIndex[i]);
				// index.push_back(listOfIndex[i+1]);
				// index.push_back(listOfIndex[i+2]);
				

			}

			
			
			
			// RID texture = createTexture(UUID);
			// vs->texture_bind(texture, 1);
			Array d;
			d.resize(VS::ARRAY_MAX);
			d[VS::ARRAY_VERTEX] = verticles;
			
			d[VS::ARRAY_NORMAL] = normals;
			d[VS::ARRAY_INDEX] = index;
			d[VS::ARRAY_COLOR] = color;

			if(true){
				vs->mesh_add_surface_from_arrays(object, VS::PRIMITIVE_TRIANGLES, d);
			}
		}
			
			
		// vs->mesh_surface_set_material(object, 0, mat->get_rid());
		
		
		Transform t;
		InstanceInfo instanceInfo;
		instanceInfo.instance = instance;
		instanceInfo.base = t;
		instances.push_back(instanceInfo);
		// if(isParent){
		// 	print_line("initialisation position parent");
		// 	positionParent=position;
		// }

		// t.translate(position);
		
		// t.scale_basis(scale);
		// t.rotate_basis(rotation, angle);

		t.rotate(Vector3(1,0,0),-0.5*Math_PI);
		
		
		// print_line("X:");
		// std::cout<<positionParent[0];

		// print_line("Y:");
		// std::cout<<positionParent[1];
		

		// print_line("Z:");
		// std::cout<<positionParent[2];
		
		
		
		vs->instance_set_transform(instance, t);
		
		// vs->sceneGateAddObject(Object,verticles, normals,index );

		for (int i =0;i<childrenUUID.size();i++)
		{
			createMesh(childrenUUID[i], object, instance);
		}


		
		// Transform t;
		// vs->instance_set_transform(instancesList[0], t);

	}


	void createMesh2(std::string UUID, bool first=false){
		listUUID.push_back(UUID);
		// Vector3 scale;
		// Vector3 position;
		// Vector3 rotation;
		// float angle;
		int nbreFaces;
		// std::vector<std::string> childrenUUID;
		
		// fillHeaders(&scale,  &position, &rotation, &angle, &nbreFaces, &childrenUUID, UUID);
		fillHeaders(&nbreFaces,UUID);

		RID object = vs->mesh_create();
		RID instance=vs->instance_create2(object, scenario);
		

		bool faceNotOkTwice = false;
		// bool faceOk = true;	
		for(int a =0;a<nbreFaces;a++){
			bool faceOk = true;	
			
			// OS::get_singleton()->print("face %i\n", i);	

			PoolVector<Vector3> verticles= * memnew(PoolVector<Vector3>);
			PoolVector<Vector3> normals=* memnew(PoolVector<Vector3>);
			PoolVector<int> index=* memnew(PoolVector<int>);
			PoolVector<Color> color=* memnew(PoolVector<Color>);
			PoolVector<Vector2> uv =* memnew(PoolVector<Vector2>);
			
			vector<real_t> listOfPos = * memnew(vector<real_t>);
			vector<real_t> listOfNorm=* memnew(vector<real_t>);
			vector<real_t> listOfIndex=* memnew(vector<real_t>);
			
			fillVectors(&listOfPos, &listOfNorm, &listOfIndex, UUID+std::to_string(a));
			
			for (int i =0;i<=(listOfPos.size()-3);i+=3){
				
				if(listOfPos[i]<1 &&!faceOk){
					faceNotOkTwice=true;
				}
				
				if(false/*listOfPos[i]<120 ||listOfPos[i]>134 /*||listOfPos[i+1]<85 ||listOfPos[i+1]>97 */){//&&faceOk
					listUUIDZero.insert(UUID);
					print_line(UUID.c_str());
					print_line(String::num_uint64(a));
					faceOk=false;
				}
				verticles.push_back(Vector3(listOfPos[i],listOfPos[i+1],listOfPos[i+2])/*.normalized()*/);
				normals.push_back(Vector3(listOfNorm[i],listOfNorm[i+1],listOfNorm[i+2])/*.normalized()*/);
				color.push_back(Color(1, 0, 0));
				uv.push_back(Vector2(0, 0));
			} 

			
			for(int i =0;i<(listOfIndex.size());i+=3){
				index.push_back(listOfIndex[i+2]);
				index.push_back(listOfIndex[i+1]);
				index.push_back(listOfIndex[i]);

				// index.push_back(listOfIndex[i]);
				// index.push_back(listOfIndex[i+1]);
				// index.push_back(listOfIndex[i+2]);
				

			}

	
			// RID texture = createTexture(UUID);
			// vs->texture_bind(texture, 1);
			Array d;
			d.resize(VS::ARRAY_MAX);
			d[VS::ARRAY_VERTEX] = verticles;
			d[VS::ARRAY_NORMAL] = normals;
			d[VS::ARRAY_INDEX] = index;
			d[VS::ARRAY_COLOR] = color;


			

			// if(faceOk){
			if(true){

				// RID object1 = vs->mesh_create();
				// RID instance1=vs->instance_create2(object1, scenario);
				// Transform t;
				// t.rotate(Vector3(1,0,0),-0.5*Math_PI);

				// vs->instance_set_transform(instance1, t);

				vs->mesh_add_surface_from_arrays(object, VS::PRIMITIVE_TRIANGLES, d);
				// vs->mesh_add_surface_from_arrays(object1, VS::PRIMITIVE_TRIANGLES, d);
			}
			
		}
		Transform t;
		
		// if(vs->mesh_get_surface_count(object) >10){
		// 	print_line("Object : "+String(UUID.c_str()));
		// 	print_line("Surfaces : "+String::num_int64(vs->mesh_get_surface_count(object)));
		// }
		// if(isParent){
		// 	print_line("initialisation position parent");
		// 	positionParent=position;
		// }

		// t.translate(position);
		
		// t.scale_basis(scale);
		// t.rotate_basis(rotation, angle);

		t.rotate(Vector3(1,0,0),-0.5*Math_PI);

		vs->instance_set_transform(instance, t);
		// InstanceInfo ii;
		// ii.instance = instance;
		// ii.base = t;
		// ii.name = UUID;
		// instances.push_back(ii);


		// if(faceOk){
		// 	vs->mesh_clear(object);
		// }
		// vs->sceneGateAddObject(Object,verticles, normals,index );
		// if(childrenUUID.size()!=0){
			
		// 	// print_line("chilldren size 111: "+String::num_int64(childrenUUID.size()));
		
		// 	for (int i =0;i<childrenUUID.size();i++)
		// 	{
				
		// 		if((std::find(listUUID.begin(), listUUID.end(), childrenUUID[i]) != listUUID.end())){
		// 			listUUIDDouble.push_back(childrenUUID[i]);
					
		// 			// print_line("doublon !!!"+String(childrenUUID[i].c_str()));
		// 			// print_line("chilldren size : "+String::num_int64(childrenUUID.size()));
		// 			// print_line("called in "+String(UUID.c_str()));
		// 			// break;
		// 		}
		// 		// print_line("On va creer :"+String(childrenUUID[i].c_str()));
		// 		// createMesh2(childrenUUID[i]);
		// 	}
			
		// }
		// print_line("END OBJ");

	}

	void fillHeaders(Vector3 *scale, Vector3 *position, Vector3 *rotation,float *angle, int *nbreFaces, std::vector<std::string> *childrenUUID,std::string key){
		std::ifstream myfile0(PATH+"header"+key+".txt");
		
		
		
		int i=0;
		int scaleAxis = 0;
		int positionAxis=0;
		// int rotationAxis=0;

		std::string a;
		while (getline(myfile0, a))
		{	
			// print_line("a:");
			// std::cout<<a;
			if(i==1||i==2||i==3){
				scale->set_axis(scaleAxis, std::stof(a));
				scaleAxis++;
			}
			if(i==4||i==5||i==6){
				position->set_axis(positionAxis, std::stof(a));
				positionAxis++;
			}


			if(i==7){
				*angle=std::stof(a);
			}


			if(i==8){
				rotation->set_axis(0, std::stof(a));
				
			}

			if(i==9){
				rotation->set_axis(1, std::stof(a));
				
			}

			if(i==10){
				rotation->set_axis(2, std::stof(a));
				
			}


			if(i==11){
				*nbreFaces=std::stof(a);
			}

			if(i>11){
				childrenUUID->push_back(a);
			}

			// if(){
			// 	positionAxis++;
			// }



			
			i++;
		}
	};

	void fillHeaders(int *nbreFaces,std::string key){
		std::ifstream myfile0(PATH+"header"+key+".txt");
		
		
		
		int i=0;
		int scaleAxis = 0;
		int positionAxis=0;
		// int rotationAxis=0;

		std::string a;
		while (getline(myfile0, a))
		{	
			*nbreFaces=std::stof(a);
		}
	};

	
	void fillVectors(vector<real_t> *verticles,vector<real_t> *normals,vector<real_t> *index, std::string key)
	{


		// Pos
		std::fstream myfile(PATH+"positions"+key+".txt", std::ios_base::in);
		// std::fstream myfile("/home/intern/Documents/Scenegate/BITBUCKET/sg/scenegate_git/build-linux-x86_64/newview/packaged/positions"+key+".txt", std::ios_base::in);

		float a;
		// vector<real_t> verticlesList;
		while (myfile >> a)
		{
			
			verticles->push_back(a);
			
		}
		// for(int i=0;i<verticlesList.size();i++){
			
		// 	std::cout<<verticlesList[i];
		// }
		

	// Normal
		std::ifstream myfile2(PATH+"normals"+key+".txt");
		// std::fstream myfile2("/home/intern/Documents/Scenegate/BITBUCKET/sg/scenegate_git/build-linux-x86_64/newview/packaged/normals"+key+".txt", std::ios_base::in);


		std::string nan= "-nan";
		std::string b;
		while (getline(myfile2, b))
		{
			if(b==nan){
				normals->push_back(0);
			}
			else{
				normals->push_back(std::stof(b));
			}

			
			
			
		}
	//Indice
		std::fstream myfile3(PATH+"indices"+key+".txt", std::ios_base::in);
		// std::fstream myfile3("/home/intern/Documents/Scenegate/BITBUCKET/sg/scenegate_git/build-linux-x86_64/newview/packaged/indices"+key+".txt", std::ios_base::in);

		
		
		while (myfile3 >> a)
		{
			
			
			index->push_back(a);
			
			
		}

		

		//return 0;
	}

	void createGodotCube1(){
		
		Vector<Vector3> vts;

		vts.push_back(Vector3(1, 1, 1));
		vts.push_back(Vector3(1, -1, 1));
		vts.push_back(Vector3(-1, 1, 1));
		vts.push_back(Vector3(-1, -1, 1));
		vts.push_back(Vector3(1, 1, -1));
		vts.push_back(Vector3(1, -1, -1));
		vts.push_back(Vector3(-1, 1, -1));
		vts.push_back(Vector3(-1, -1, -1));

		Geometry::MeshData md;
		Error err = QuickHull::build(vts, md);
		print_line("ERR: " + itos(err));
		cube = vs->mesh_create();
		vs->mesh_add_surface_from_mesh_data(cube, md);

		cubeInstance = vs->instance_create2(cube, scenario);


		// Transform t;
		
		// InstanceInfo instanceInfo;
		// instanceInfo.instance = test_cube;
		// instanceInfo.base = t;
		
		// instances.push_back(instanceInfo);
		vs->instance_set_transform(cubeInstance, cubeTrans);
	}

	void createGodotCube2(){
		
		Vector<Vector3> vts;

		vts.push_back(Vector3(1, 1, 1));
		vts.push_back(Vector3(1, -1, 1));
		vts.push_back(Vector3(-1, 1, 1));
		vts.push_back(Vector3(-1, -1, 1));
		vts.push_back(Vector3(1, 1, -1));
		vts.push_back(Vector3(1, -1, -1));
		vts.push_back(Vector3(-1, 1, -1));
		vts.push_back(Vector3(-1, -1, -1));

		Geometry::MeshData md;
		Error err = QuickHull::build(vts, md);
		print_line("ERR: " + itos(err));
		cube2 = vs->mesh_create();
		vs->mesh_add_surface_from_mesh_data(cube2, md);

		cubeInstance2 = vs->instance_create2(cube2, scenario);

		cubeTrans2.scale(Vector3(0.5,0.5,0.5));
		// Transform t;
		
		// InstanceInfo instanceInfo;
		// instanceInfo.instance = test_cube;
		// instanceInfo.base = t;
		
		// instances.push_back(instanceInfo);
		vs->instance_set_transform(cubeInstance2, cubeTrans2);
	}

	void createGodotCube3WithTexture(std::string UUID){
		
		Vector<Vector3> vts;

		vts.push_back(Vector3(1, 1, 1));
		vts.push_back(Vector3(1, -1, 1));
		vts.push_back(Vector3(-1, 1, 1));
		vts.push_back(Vector3(-1, -1, 1));
		vts.push_back(Vector3(1, 1, -1));
		vts.push_back(Vector3(1, -1, -1));
		vts.push_back(Vector3(-1, 1, -1));
		vts.push_back(Vector3(-1, -1, -1));

		Geometry::MeshData md;
		Error err = QuickHull::build(vts, md);
		print_line("ERR: " + itos(err));
		cube = vs->mesh_create();
		vs->mesh_add_surface_from_mesh_data(cube, md);
		// ImageTexture* texture = createTexture(UUID);
		// RID texture = createTextureFromImg("res://Test2.png");
		Ref<ImageTexture> texture2 = createTextureFromImg2("res://Test2.png");
		RID texture = vs->get_test_texture();
		cube2 = vs->get_test_cube();
		RID material = vs->material_create();
		
		vs->material_set_param(material, "albedo_texture", texture);
		vs->material_set_param(material, "albedo", Color(0, 1, 0, 1));
		vs->material_set_param(material, "specular", 0.5);
		vs->material_set_param(material, "metallic", 0.0);
		vs->material_set_param(material, "roughness", 1.0);
		vs->material_set_param(material, "uv1_offset", Vector3(0, 0, 0));
		vs->material_set_param(material, "uv1_scale", Vector3(1, 1, 1));
		vs->material_set_param(material, "uv2_offset", Vector3(0, 0, 0));
		vs->material_set_param(material, "uv2_scale", Vector3(1, 1, 1));
		vs->material_set_param(material, "alpha_scissor_threshold", 0.98);

		Ref<SpatialMaterial> mat = memnew(SpatialMaterial);
		mat->set_flag(SpatialMaterial::FLAG_ALBEDO_FROM_VERTEX_COLOR, true);
		mat.instance();
		mat->set_albedo(Color(0.7, 0.7, 1.0, 0.2));
		mat->set_flag(SpatialMaterial::FLAG_UNSHADED, true);
		mat->set_feature(SpatialMaterial::FEATURE_TRANSPARENT, true);
		mat->set_texture(SpatialMaterial::TEXTURE_ALBEDO, texture2);
		RID mat2 = mat->get_rid();

		

		// Ref<Material> *mat3 =

		vs->mesh_surface_set_material(cube, 0, mat2);
		// RID sky = vs->sky_create();
		// vs->sky_set_texture(sky, texture, 1);

		// SpatialMaterial *material = memnew(SpatialMaterial);
		// material->set_texture(SpatialMaterial::TEXTURE_ALBEDO, texture);
		
		cubeInstance = vs->instance_create2(cube, scenario);
		// vs->instance_set_surface_material(cubeInstance, 0, material);
		// skyInstance = vs->instance_create2(sky, scenario);
		vs->instance_set_surface_material(cubeInstance, 0, material);
		// Transform t;
		
		// InstanceInfo instanceInfo;
		// instanceInfo.instance = test_cube;
		// instanceInfo.base = t;
		
		// instances.push_back(instanceInfo);
		vs->instance_set_transform(cubeInstance, cubeTrans);
	}


	void createAvatar(){
		avatar= vs->mesh_create();
		avatarInstance=vs->instance_create2(avatar, scenario);
	}

	void updateAvatar(){
		vs->mesh_clear(avatar);
		createMesh("e6fa52ce-14c4-4bbf-8d3a-d0615b8bb693", avatar, avatarInstance);
	}
};

MainLoop *test() {

	return memnew(TestMainLoop);
}

} // namespace TestRender




