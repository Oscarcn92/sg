#include "godotWorld.h"

class GodotAPI
{
private:
   std::list<ObjectToUpdate> createFromtext();


public:
    GodotAPI();
    void meshThread();
    void camThread();
    GodotWorld world;

    //Only for test, need to change depending on your repository
    std::string PATH = "/home/svwd/Documents/Pierre/sg/scenegate_git/build-linux-x86_64/newview/packaged/";
};
