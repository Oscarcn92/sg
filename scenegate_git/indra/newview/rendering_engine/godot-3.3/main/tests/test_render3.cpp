#include "test_render3.h"

#include "main/performance.h"

#include "core/math/math_funcs.h"
#include "core/math/quick_hull.h"
#include "core/os/keyboard.h"
#include "core/os/main_loop.h"
#include "core/os/os.h"
#include "core/print_string.h"
#include "servers/visual_server.h"
#include "scene/resources/mesh.h"
#include "core/array.h"
#include "core/image.h"
#include <bits/stdc++.h>



#include <string>
#include <iostream>
#include <fstream>


#include "godotAPI.h"

namespace TestRender3 {

class TestMainLoop : public MainLoop {

    //variables for input event
    bool stop = false;
    bool renderUpdate = true;
    Transform t;
    RID camera;

    RID scenario;
    RID light;
    Transform cameraT;
    
    Performance *perfs = Performance::get_singleton();

	VisualServer *vs = VisualServer::get_singleton();

    bool quit;

	bool scenegate=true;
    GodotAPI godotAPI;

    public:
	virtual void input_event(const Ref<InputEvent> &p_event) {
		RID camera = godotAPI.world.getCamera();
		RID viewport =godotAPI.world.getViewport();
		 
		if (p_event->is_pressed()){
			// float r= t.get_basis()[0]
			
			// quit = true;
			print_line(p_event->as_text());
			stop=true;
		}
		else{
			return;
		}
		//DEBUG INFOS :
		if (p_event->as_text()=="A"){
		
			std::vector<String> RenderInfos ={

				"INFO_OBJECTS_IN_FRAME",
				"INFO_VERTICES_IN_FRAME",
				"INFO_MATERIAL_CHANGES_IN_FRAME",
				"INFO_SHADER_CHANGES_IN_FRAME",
				"INFO_SURFACE_CHANGES_IN_FRAME",
				"INFO_DRAW_CALLS_IN_FRAME",
				"INFO_2D_ITEMS_IN_FRAME",
				"INFO_2D_DRAW_CALLS_IN_FRAME",
				"INFO_USAGE_VIDEO_MEM_TOTAL",
				"INFO_VIDEO_MEM_USED",
				"INFO_TEXTURE_MEM_USED",
				"INFO_VERTEX_MEM_USED",
			};
			for(int i = 0;i !=12;i++){
			// OS::get_singleton()->print(perfs->get_monitor_name(Performance::Monitor(i))+"%f\n", perfs->get_monitor(Performance::Monitor(i)));
			print_line(RenderInfos[i]+":"+String::num_int64(vs->get_render_info(VisualServer::RenderInfo(i))));
			// print_line("ok");
			}
			print_line("FPS :"+String::num_real(perfs->get_monitor(Performance::Monitor::TIME_FPS)));
			// OS::get_singleton()->print(vs->is_render_loop_enabled());

		}
		//update or not 
		if (p_event->as_text()=="U"){
		
			renderUpdate= !renderUpdate;
		}

		
		//ROTA Lumiere
		if (p_event->as_text()=="D"){
		
			t.rotate(Vector3(0, 1, 0), 0.1);
			vs->instance_set_transform(light, t);
		}

		if (p_event->as_text()=="Q"){
		
			t.rotate_basis(Vector3(0, 1, 0), -0.1);
			vs->instance_set_transform(light, t);
		}

		if (p_event->as_text()=="Z"){
		
			t.rotate_basis(Vector3(1, 0, 0), 0.1);
			vs->instance_set_transform(light, t);
		}

		if (p_event->as_text()=="S"){
		
			t.rotate_basis(Vector3(1, 0, 0), -0.1);
			vs->instance_set_transform(light, t);
		}

		//rota cam
		if (p_event->as_text()=="I"){
			
			print_line(cameraT.operator String());
			
		}

		if (p_event->as_text()=="P"){
		
			cameraT.set_origin(Vector3(112,28,-66));
			// cameraT.set_origin(Vector3(116.175804, 29.519739, -26.835373));
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="L"){
		
			scenegate = !scenegate;
		}

		if (p_event->as_text()=="F"){
		
			cameraT.rotate_basis(Vector3(0, 1, 0), 0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="H"){
		
			cameraT.rotate_basis(Vector3(0, 1, 0), -0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="T"){
		
			cameraT.rotate_basis(Vector3(1, 0, 0), 0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="G"){
		
			cameraT.rotate_basis(Vector3(1, 0, 0), -0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="R"){
		
			cameraT.rotate_basis(Vector3(0, 0, 1), 0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="Y"){
		
			cameraT.rotate_basis(Vector3(0, 0, 1), -0.01);
			
			vs->camera_set_transform(camera, cameraT);
		}

		
//DEPLACEMENTS CAMERA

		if (p_event->as_text()=="Up"){
		
			cameraT.translate(Vector3(0, 1, 0));
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="Down"){
		
			cameraT.translate(Vector3(0,-1, 0));
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="Left"){
		
			cameraT.translate(Vector3(-1, 0, 0));
			vs->camera_set_transform(camera, cameraT);
			
		}

		if (p_event->as_text()=="Right"){
		
			cameraT.translate(Vector3(1, 0, 0));
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="N"){
		
			cameraT.translate(Vector3(0, 0, 0.1));
			
			vs->camera_set_transform(camera, cameraT);
		}

		if (p_event->as_text()=="Comma"){
		
			cameraT.translate(Vector3(0, 0, -0.1));
			
			vs->camera_set_transform(camera, cameraT);
		}

	
			
	}

    virtual void init() {

		print_line("INITIALIZING TEST RENDER");
		godotAPI = GodotAPI();
		scenario = godotAPI.world.getScenario();
		RID camera = godotAPI.world.getCamera();
		RID viewport =godotAPI.world.getViewport();

		/* LIGHT */
		RID lightaux = vs->directional_light_create();
		vs->light_set_shadow(lightaux, true);
		light = vs->instance_create2(lightaux, scenario);
		Transform lla;
		lla.translate(Vector3(0, 100, 0));
		lla.rotate_basis(Vector3(1, 0, 0),-0.5*Math_PI);
		vs->instance_set_transform(light, lla);
		

		/* CAMERA */

		//camera = vs->camera_create();
		//RID viewport = vs->viewport_create();
		Size2i screen_size = OS::get_singleton()->get_window_size();
		vs->viewport_set_size(viewport, screen_size.x, screen_size.y);
		vs->viewport_attach_to_screen(viewport, Rect2(Vector2(), screen_size));
		vs->viewport_set_active(viewport, true);
		vs->viewport_attach_camera(viewport, camera);
		vs->viewport_set_scenario(viewport, scenario);
		vs->viewport_set_render_direct_to_screen(viewport, true);
		
		
		cameraT.translate(Vector3(0, 0, 10));
		vs->camera_set_transform(camera, cameraT);
		

		//ENVIRONMENT 

		RID env = vs->environment_create ( );
		
		// vs->environment_set_adjustment(env, true, 10, 10, 10);
		vs->environment_set_bg_color(env, Color(0.7, 0.7, 1.0));
		vs->environment_set_bg_energy (  env, 1 );
		vs->environment_set_ambient_light(env, Color(0.7, 0.7, 1.0), 0.1);
		
		vs->scenario_set_environment(scenario,env );

        godotAPI.meshThread();
		quit = false;
	}


    virtual bool iteration(float p_time) {

		if(scenegate){
			godotAPI.meshThread();

		}

		godotAPI.camThread();
		return quit;
	}


    virtual bool idle(float p_time) {
		return quit;
	}

	virtual void finish() {
	}

};

MainLoop *test() {

	return memnew(TestMainLoop);
}

} // namespace TestRender3

