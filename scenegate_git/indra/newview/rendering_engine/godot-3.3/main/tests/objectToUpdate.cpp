#include "objectToUpdate.h"
#include <string>
#include <iostream>
#include <fstream>


using namespace std;

void ObjectToUpdate::getFromString(std::string UUID)
{

    this->name =UUID;
    this->updateType =updateType::GEOMETRY;

    //Just for tests, need to change depending 
    string PATH = "/home/svwd/Documents/Pierre/sg/scenegate_git/build-linux-x86_64/newview/packaged/Meshes/";

    std::ifstream myfile0(PATH+"header"+UUID+".txt");

    //TO get the face
    std::string str;
    while (getline(myfile0, str))
    {	
        this->nbreFaces=std::stof(str);
    }
	
    //TTo get position, normal index
    for(int i =0;i<this->nbreFaces;i++){
        
        string key = UUID+std::to_string(i);
        Face face;

        // Pos
        std::fstream myfile(PATH+"positions"+key+".txt", std::ios_base::in);
        float a;
        while (myfile >> a)
        {
            face.verticles.push_back(a);  
        }
        

        // Normal
        std::ifstream myfile2(PATH+"normals"+key+".txt");

        std::string nan= "-nan";
        std::string b;
        while (getline(myfile2, b))
        {
            if(b==nan)
            {
                face.normals.push_back(0);
            }
            else
            {
                face.normals.push_back(std::stof(b));
            }       
        }

        //Indice
        std::fstream myfile3(PATH+"indices"+key+".txt", std::ios_base::in);
  
        while (myfile3 >> a)
        {   
            face.indices.push_back(a);   
        }



        faces.push_back(face);
    }
}
