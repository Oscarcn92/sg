#include "godotAPI.h"
#include <list>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

// #include "/home/svwd/Documents/Gauthier/SG/scenegate_git/indra/newview/rendering_engine/API/objectToUpdate.h"
// #include "/home/svwd/Documents/Gauthier/SG/scenegate_git/indra/newview/rendering_engine/API/renderAPI.h"



GodotAPI::GodotAPI()
{
    this->world = GodotWorld();
}

//thread that loads the camera information from the txt file
void GodotAPI::camThread()
{
	std::fstream camfile;
	std::string basistxt;
	std::vector<float> coords;
    bool canupdate=true;
    std::fstream state;
	std::string a;
	state.open (PATH+"camstate.txt", std::ios_base::in);
	while (getline(state, a)) {
        //print_line("a1 :"+String(a.c_str()));

		if (a == "Writing") {
			canupdate = false;
            //print_line("a2 :"+String(a.c_str()));

		}
	}
    state.close();

	if (canupdate) {
        state.open(PATH+"camstate.txt", std::ios_base::out |std::ios_base::trunc);
        state << "Reading";
        state.close();

		camfile.open(PATH+"caminfo.txt", std::ios_base::in);
		while (getline(camfile, basistxt)) {
			coords.push_back(std::stof(basistxt));
		}

        Vector3 Camx = Vector3(coords[0], coords[2], -coords[1]);
        Vector3 Camy = Vector3(-coords[6], -coords[8], coords[7]);
        Vector3 Camz = Vector3(coords[3], coords[5], -coords[4]);
        Basis basecam = Basis(Camx, Camy, Camz);
        basecam.rotate(Vector3(1, 0, 0), Math_PI);
        basecam.rotate(Vector3(0, 1, 0), 0.5 * Math_PI);
        basecam.transpose();

		Vector3 Origin = Vector3(coords[9], coords[11], -coords[10]);
		Transform camera2b = Transform(basecam, Origin);

		this->world.updateCamera(camera2b);
        state.open(PATH+"camstate.txt", std::ios_base::out |std::ios_base::trunc);
        state << "Read";
        state.close();

	}
}


void GodotAPI::meshThread()
{
   
    std::fstream output_file;

    bool test=false;
    
    std::string a;
    output_file.open(PATH+"askForUpdate.txt", std::ios_base::in);
    while (getline(output_file, a)){ 
        if(a=="ENDWRITE"){
            test=true;
        }
    }
    
    output_file.close();
    if(test){
        std::list<ObjectToUpdate> objectsToUpdate=createFromtext();

        for (ObjectToUpdate const& object : objectsToUpdate) {
            switch(object.updateType) {
                case ObjectToUpdate::UpdateType::CREATION :
                    this->world.createObject(object);
                    break;
                case ObjectToUpdate::UpdateType::DESTRUCTION:
                    this->world.killObject(object);
                    break;
                case ObjectToUpdate::UpdateType::POSITION:
                    // code block
                    break;
                
                case ObjectToUpdate::UpdateType::GEOMETRY:
                    this->world.updateGeometry(object);
                    break;

                case ObjectToUpdate::UpdateType::REBUILD:
                    // code block
                    break;
                
            }
        }

        //We ask for a new update then
        output_file.open(PATH+"askForUpdate.txt", std::ios_base::out |std::ios_base::trunc);
        output_file << "isAsking";
        output_file.close();
    }

}

std::list<ObjectToUpdate> GodotAPI::createFromtext()
{
    std::list<ObjectToUpdate> objList;
    std::fstream myfile(PATH+"Meshes/objListUUID.txt", std::ios_base::in);

    std::string a;

    while (getline(myfile, a))
    {
        ObjectToUpdate objectToUpdate;
        objectToUpdate.getFromString(a);
        objList.push_back(objectToUpdate);            
    }


    std::fstream myfile1(PATH+"Meshes/objListDestroy.txt", std::ios_base::in);

    std::string b;

    while (getline(myfile1, b))
    {
        ObjectToUpdate objectToUpdate;
        objectToUpdate.name=b;
        objectToUpdate.updateType =ObjectToUpdate::updateType::DESTRUCTION;
        objList.push_back(objectToUpdate);            
    }

    return objList;

}


