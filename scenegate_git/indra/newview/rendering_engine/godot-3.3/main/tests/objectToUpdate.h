#include <string>
#include <vector>
#include <list>


class ObjectToUpdate
{

public:

    std::string name;
    std::vector<float> position;
    std::vector<float> scale;
    std::vector<float> rotation;
    float angle;

    typedef enum updateType
	{
		POSITION  	= 0,
		GEOMETRY 	= 1,
		CREATION		= 2,
		DESTRUCTION		= 3,
		REBUILD		= 4,
		
	}UpdateType;

    UpdateType updateType;
    
    int nbreFaces = 0;

    struct Face {

        std::vector<float> verticles;
        std::vector<float> normals;
        std::vector<float> indices; 
        
    };

    std::vector<Face> faces;

    void getFromString(std::string UUID);

};
