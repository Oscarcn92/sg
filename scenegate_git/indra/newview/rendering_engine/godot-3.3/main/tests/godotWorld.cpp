#include "godotWorld.h"
// #include "/home/svwd/Documents/Gauthier/SG/scenegate_git/indra/newview/rendering_engine/API/objectToUpdate.h"

GodotWorld::GodotWorld(){
    this->scenario =vs->scenario_create();
    this->camera =vs->camera_create();
    this->viewport =vs->viewport_create();   
  
}

void GodotWorld::setScenario(RID _scenario){
    this->scenario =_scenario;
}

RID GodotWorld::getScenario(){
    return this->scenario;
}

RID GodotWorld::getViewport(){
    return this->viewport;
}

RID GodotWorld::getCamera(){
    return this->camera;
}

void GodotWorld::createObject(ObjectToUpdate object){

    
    bool objExists = false;

    Object Gobject = Object();


    //check if the world already knows this obj, just to be sure
    for (Object const& obj : this->objectsList){
        if(obj.name ==object.name){
            objExists=true;
            Gobject=obj;
            break;
        }
    }
    if(!objExists){//We create a new obj
        
        Gobject.name =object.name;

        RID geom = createGeometry(object);
        Gobject.instance = vs->instance_create2(geom, this->scenario);
        
        //We need to apply rotation as Godot and scenegate are not in the same referentiel
        Transform t;
        t.rotate(Vector3(1,0,0),-0.5*Math_PI);
        vs->instance_set_transform(Gobject.instance, t);

        this->objectsList.push_back(Gobject);
    }
    else{
        //TODO : decide what we do in this case : if notification of obj creation but obj already created
    }  
}

void GodotWorld::updateCamera(Transform toUse){
    vs->camera_set_transform(camera, toUse);
}


void GodotWorld::updateGeometry(ObjectToUpdate object){

    
    bool objExists = false;

    Object Gobject = Object();

    //check if the world already knows this obj, just to be sure
    for (Object const& obj : this->objectsList){
        if(obj.name ==object.name){
            objExists=true;
            Gobject=obj;
            break;
        }
    }
    if(!objExists){//We create a new obj
        createObject(object);
    }
    else{
        RID geom = createGeometry(object);
        vs->instance_set_base(Gobject.instance, geom);
    }
}


void GodotWorld::updatePosition(ObjectToUpdate object){
  
    bool objExists = false;

    Object Gobject = Object();

    //check if the world already knows this obj, just to be sure
    for (Object const& obj : this->objectsList){
        if(obj.name ==object.name){
            objExists=true;
            Gobject=obj;
            break;
        }
    }
    if(!objExists){//We create a new obj
        createObject(object);
        
        //creating the object set the position by def to (0,0)(because position of meshes are already calculated in SG noramlly); so we have to force it
        updatePosition(object);
    }
    else{
        Transform t = createTransform(object);        
        vs->instance_set_transform(Gobject.instance, t);        
    }  
}


void GodotWorld::killObject(ObjectToUpdate object){

    
    bool objExists = false;

    Object Gobject =  Object();

    //check if the world already knows this obj, just to be sure
    for (Object const& obj : this->objectsList){
        if(obj.name ==object.name){
            objExists=true;
            Gobject=obj;
            break;
        }
    }
    if(objExists){
        vs->free(Gobject.instance);
        this->objectsList.remove(Gobject);
    }
    else{
        //Normally nothing to do if object non existing
    }

    
}



RID GodotWorld::createGeometry(ObjectToUpdate object){

    RID mesh = vs->mesh_create();

    for (ObjectToUpdate::Face const& face : object.faces) {
        
        PoolVector<Vector3> verticles= * memnew(PoolVector<Vector3>);
        PoolVector<Vector3> normals=* memnew(PoolVector<Vector3>);
        PoolVector<int> index=* memnew(PoolVector<int>);
        
        //On remplit les poolVectors
        for (int i =0;i<=(face.verticles.size()-3);i+=3){
            verticles.push_back(Vector3(face.verticles[i],face.verticles[i+1],face.verticles[i+2]));
			normals.push_back(Vector3(face.normals[i],face.normals[i+1],face.normals[i+2]));
        }

        for (int i =0;i<=(face.indices.size()-3);i+=3){
            index.push_back(face.indices[i+2]);
            index.push_back(face.indices[i+1]);
            index.push_back(face.indices[i]);
        }

        Array d;
        d.resize(VS::ARRAY_MAX);
        d[VS::ARRAY_VERTEX] = verticles;
        d[VS::ARRAY_NORMAL] = normals;
        d[VS::ARRAY_INDEX] = index;


        vs->mesh_add_surface_from_arrays(mesh, VS::PRIMITIVE_TRIANGLES, d);
    }

    return mesh;
}

Transform GodotWorld::createTransform(ObjectToUpdate object){

    Transform t;

    Vector3 scale =  Vector3(object.scale[0], object.scale[1], object.scale[2]);
    Vector3 position = Vector3(object.position[0], object.position[1], object.position[2]);
    Vector3 rotation =  Vector3(object.rotation[0], object.rotation[1], object.rotation[2]);
    float angle = object.angle;
    
    t.translate(position);	
    t.scale_basis(scale);
    t.rotate_basis(rotation, angle);
    t.rotate(Vector3(1,0,0),-0.5*Math_PI);
    
    
    return t;
}