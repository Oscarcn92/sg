#include "servers/visual_server.h"
#include "objectToUpdate.h"
#include "core/os/os.h"

// #include "/home/svwd/Documents/Gauthier/SG/scenegate_git/indra/newview/rendering_engine/API/objectToUpdate.h"
// #include "/home/svwd/Documents/Gauthier/SG/scenegate_git/indra/newview/rendering_engine/API/renderAPI.h"


class GodotWorld
{
private:
//TODO : ces inits ds le constr
    VisualServer *vs = VisualServer::get_singleton();
    RID scenario ;
    RID camera;
    RID viewport;
    struct Object {

        RID instance;
        std::string name;

        bool operator == (const Object& s) const { return name == s.name; }
    };

    std::list<Object> objectsList;

    RID createGeometry(ObjectToUpdate object);
    Transform createTransform(ObjectToUpdate object);


public:
    
    void setScenario(RID _scenario);
    RID getScenario();
    RID getCamera();
    RID getViewport();
    GodotWorld();

    void updateCamera(Transform toUse);
    void createObject(ObjectToUpdate object);

    void updateGeometry(ObjectToUpdate object);
    void updatePosition(ObjectToUpdate object);
    void killObject(ObjectToUpdate object);
};