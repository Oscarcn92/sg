/*************************************************************************/
/*  test_render.cpp                                                      */
/*************************************************************************/
/*                       This file is part of:                           */
/*                           GODOT ENGINE                                */
/*                      https://godotengine.org                          */
/*************************************************************************/
/* Copyright (c) 2007-2021 Juan Linietsky, Ariel Manzur.                 */
/* Copyright (c) 2014-2021 Godot Engine contributors (cf. AUTHORS.md).   */
/*                                                                       */
/* Permission is hereby granted, free of charge, to any person obtaining */
/* a copy of this software and associated documentation files (the       */
/* "Software"), to deal in the Software without restriction, including   */
/* without limitation the rights to use, copy, modify, merge, publish,   */
/* distribute, sublicense, and/or sell copies of the Software, and to    */
/* permit persons to whom the Software is furnished to do so, subject to */
/* the following conditions:                                             */
/*                                                                       */
/* The above copyright notice and this permission notice shall be        */
/* included in all copies or substantial portions of the Software.       */
/*                                                                       */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,       */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF    */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.*/
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY  */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,  */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE     */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                */
/*************************************************************************/

#include "test_render2.h"

#include "core/math/math_funcs.h"
#include "core/math/quick_hull.h"
#include "core/os/keyboard.h"
#include "core/os/main_loop.h"
#include "core/os/os.h"
#include "core/print_string.h"
#include "servers/visual_server.h"

#include "core/set.h"


namespace TestRender2 {

class TestMainLoop : public MainLoop {
	
	RID instance;
	RID camera;
	RID viewport;
	RID light;
	RID scenario;

	RID cube;
	Transform cube_t;


	Map<RID, Geometry::MeshData> _meshes;

	RWLock _lock;

	float ofs;
	bool quit;

protected:

	RID create_mesh(VisualServer * vs)
	{
		RID test_cube = vs->get_test_cube();
		Vector<Vector3> vts;

		vts.push_back(Vector3(1, 1, 1));
		vts.push_back(Vector3(1, -1, 1));
		vts.push_back(Vector3(-1, 1, 1));
		vts.push_back(Vector3(-1, -1, 1));
		vts.push_back(Vector3(1, 1, -1));
		vts.push_back(Vector3(1, -1, -1));
		vts.push_back(Vector3(-1, 1, -1));
		vts.push_back(Vector3(-1, -1, -1));

		Geometry::MeshData md;
		Error err = QuickHull::build(vts, md);
		print_line("ERR: " + itos(err));
		test_cube = vs->mesh_create();
		vs->mesh_add_surface_from_mesh_data(test_cube, md);

		test_cube = vs->instance_create2(test_cube, scenario);

		return test_cube;
	}

	Geometry::MeshData GetMeshCube()
	{
		Vector<Vector3> vts;

		vts.push_back(Vector3(1, 1, 1));
		vts.push_back(Vector3(1, -1, 1));
		vts.push_back(Vector3(-1, 1, 1));
		vts.push_back(Vector3(-1, -1, 1));
		vts.push_back(Vector3(1, 1, -1));
		vts.push_back(Vector3(1, -1, -1));
		vts.push_back(Vector3(-1, 1, -1));
		vts.push_back(Vector3(-1, -1, -1));

		Geometry::MeshData md;
		Error err = QuickHull::build(vts, md);
		print_line("ERR: " + itos(err));
		return md;
	}

	void RenderMesh(RID id, Geometry::MeshData md, Transform t)
	{
		if (!Contains(id))
		{
			VisualServer::get_singleton()->mesh_add_surface_from_mesh_data(id, md);
			VisualServer::get_singleton()->instance_create2(id, scenario);
			_meshes[id] = md;
		}
		VisualServer::get_singleton()->instance_set_transform(id, t);
	}

	bool Contains(const RID &id) {

		_lock.read_lock();
		bool b = _meshes.has(id);
		_lock.read_unlock();

		return b;
	}

public:
	/* INPUT EVENT
	virtual void input_event(const Ref<InputEvent> &p_event) {

		if (p_event->is_pressed())
			quit = true;
	}
	*/

	virtual void init() {

		print_line("INITIALIZING TEST RENDER");
		VisualServer *vs = VisualServer::get_singleton();
		
		scenario = vs->scenario_create();
		
		cube = create_mesh(vs);
		cube_t = Transform(Basis(), Vector3(0, 20, 1));
		vs->instance_set_transform(cube, cube_t);

		// ****************************

		
		// Geometry::MeshData mesh = GetMeshCube();

		// for (int i=0; i<5; i++)
		// {
		// 	RID rid = VisualServer::get_singleton()->mesh_create();

		// 	Transform tr = Transform(Basis(), Vector3(0, i, 0));

		// 	RenderMesh(rid, mesh, tr);
		// }
		

		// ****************************

		camera = vs->camera_create();

		viewport = vs->viewport_create();
		Size2i screen_size = OS::get_singleton()->get_window_size();
		vs->viewport_set_size(viewport, screen_size.x, screen_size.y);
		vs->viewport_attach_to_screen(viewport, Rect2(Vector2(), screen_size));
		vs->viewport_set_active(viewport, true);
		vs->viewport_attach_camera(viewport, camera);
		vs->viewport_set_scenario(viewport, scenario);
		vs->camera_set_transform(camera, Transform(Basis(), Vector3(0, 20, 30)));
		vs->camera_set_perspective(camera, 60, 0.1, 1000);

		RID lightaux;

		lightaux = vs->directional_light_create();
		//vs->light_set_color( lightaux, VisualServer::LIGHT_COLOR_AMBIENT, Color(0.0,0.0,0.0) );
		vs->light_set_color(lightaux, Color(1.0, 1.0, 1.0));
		//vs->light_set_shadow( lightaux, true );
		light = vs->instance_create2(lightaux, scenario);
		Transform lla;
		//lla.set_look_at(Vector3(),Vector3(1,-1,1),Vector3(0,1,0));
		lla.set_look_at(Vector3(), Vector3(-0.000000, -0.836026, -0.548690), Vector3(0, 1, 0));

		vs->instance_set_transform(light, lla);

		lightaux = vs->omni_light_create();
		//vs->light_set_color( lightaux, VisualServer::LIGHT_COLOR_AMBIENT, Color(0.0,0.0,1.0) );
		vs->light_set_color(lightaux, Color(1.0, 1.0, 0.0));
		vs->light_set_param(lightaux, VisualServer::LIGHT_PARAM_RANGE, 4);
		vs->light_set_param(lightaux, VisualServer::LIGHT_PARAM_ENERGY, 8);
		//vs->light_set_shadow( lightaux, true );
		//light = vs->instance_create( lightaux );

		ofs = 0;
		quit = false;
	}

	virtual bool iteration(float p_time) {

		ofs += p_time * 0.1;



		return quit;
	}

	virtual bool idle(float p_time) {
		return quit;
	}

	virtual void finish() {
	}
};

MainLoop *test() {

	return memnew(TestMainLoop);
}
} // namespace TestRender2
