
# Location : indra/newview/rendering_engine/llrender

**brief :** Font library wrapper, to modify many parameters of the written characters everywhere

# Imports :

- llstring.h
- llstl.h *(from .h file)*
- llpointer.h *(from .h file)*
- llmath.h
- llimagegl.h *(from .h file)*`
- llimage.h
- llgl.h
- llfontgl.h
- llfontfreetype.h
- llfontbitmapcache.h *(from .h file)*
- llfontbitmapcache.h
- llerror.h
- linden_common.h
- FT_FREETYPE_H
- &lt;freetype2/ft2build.h&gt;
- &lt;boost/container/flat_map.hpp&gt; *(from .h file)*

# Classes : LLFontManager, LLFontFreetype

<span style="color:green">// Hack.  FT_Face is just a typedef for a pointer to a struct,
// but there's no simple forward declarations file for FreeType, 
// and the main include file is 200K.  
// We'll forward declare the struct here.  JC</span>

#### struct FT_FaceRec_
#### typedef struct FT_FaceRec_* LLFT_Face

#### extern LLFontManager *gFontManagerp
&nbsp;

#### struct LLFontGlyphInfo
{\
LLFontGlyphInfo(U32 index); \
U32 mGlyphIndex; \
// Metrics\
S32 mWidth;	<span style="color:green">// In pixels</span>\
S32 mHeight;	<span style="color:green">// In pixels</span>\
F32 mXAdvance;	<span style="color:green">// In pixels</span>\
F32 mYAdvance;	<span style="color:green">// In pixels</span>\
<span style="color:green">// Information for actually rendering</span>\
S32 mXBitmapOffset;	<span style="color:green">// Offset to the origin in the bitmap</span>\
S32 mYBitmapOffset;	<span style="color:green">// Offset to the origin in the bitmap</span>\
S32 mXBearing;	<span style="color:green">// Distance from baseline to left in pixels</span>\
S32 mYBearing;	<span style="color:green">// Distance from baseline to top in pixels</span>\
S32 mBitmapNum; <span style="color:green">// Which bitmap in the bitmap cache contains this glyph</span>\
}

# Class : LLFontManager

## public : 

### --------------------- ATTRIBUTS ---------------------

None 

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### static void cleanupClass()
Delete the instance of the class that the parent contains (singleton ?).
&nbsp;
#### static void initClass()
Create a new instance of the class inside it only if not already existing, certaily to make it a singleton.Besides, this would explain why the constructor is private.\
Called in llui_libtest.cpp (1), llviewerwindow.cpp (2).
&nbsp;

## private :

### --------------------- ATTRIBUTS ---------------------

None

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

None

# Class : LLFontFreetype

## public : 

### --------------------- ATTRIBUTS ---------------------

#### typedef std::vector<LLPointer<LLFontFreetype> > font_vector_t
Used to stock a list of fallback fonts to look for glyphs in (for Unicode chars).
Also used in llfontregistry.cpp

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### LLFontFreetype()
Constructor. Write in LLTrace::MemTrackable. Reserve memory for glyphs.
Called in llfontgl.cpp

#### BOOL loadFace(const std::string& filename, F32 point_size, F32 vert_dpi, F32 horz_dpi, S32 components, BOOL is_fallback)
Initialize the parameters (place for every character, police etc... of the font).\
Called in llfontgl.cpp (1).
&nbsp;
#### void setFallbackFonts(const font_vector_t &font)
Just a setter for mFallbackFonts.\
Called in llfontregistry.cpp (1).
&nbsp;
#### const font_vector_t &getFallbackFonts() const
Just a getter for mFallbackFonts.\
&nbsp;

<span style="color:green">// # next ones : Global font metrics - in units of pixels</span>

#### F32 getLineHeight() const
Just a getter for mLineHeight.\
&nbsp;

#### F32 getAscenderHeight() const
Just a getter for mAscender.\
Called in llfontgl.cpp (5).
&nbsp;

#### F32 getDescenderHeight() const
Just a getter for mDescender.\
Called in llfontgl.cpp (5).
&nbsp;

#### F32 getXAdvance(llwchar wc) const
Try to return the 'position' (not sure) of the character parameter.
&nbsp;

#### F32 getXAdvance(const LLFontGlyphInfo* glyph) const
Return the 'position' (not sure) of the glyph parameter.\
Called in llfontgl.cpp (2).
&nbsp;

#### F32 getXKerning(llwchar char_left, llwchar char_right) const
Get the kerning between the two characters.\
Called in llfontgl.cpp (1).
&nbsp;

#### F32 getXKerning(const LLFontGlyphInfo* left_glyph_info, const LLFontGlyphInfo* right_glyph_info) const
Get the kerning between the two characters using glyphs infos.\
Called in llfontgl.cpp (4).
&nbsp;

#### LLFontGlyphInfo* getGlyphInfo(llwchar wch) const
Try to find the character and give a feed back of it's infos, or render it if not existing.\
Called in llfontgl.cpp (9).
&nbsp;

#### void reset(F32 vert_dpi, F32 horz_dpi)
Reset dpi, cache and fallbackfonts if existing.\
Called in llfontgl.cpp (1).
&nbsp;

#### void destroyGL()
Just call LLFontBitmapCache.destroyGL() .\
Called in llfontgl.cpp (1).
&nbsp;

#### const std::string& getName() const
Just a getter for mName.\
&nbsp;

#### const LLFontBitmapCache* getFontBitmapCache() const
Just a getter for mFontBitmapCachep.\
&nbsp;

#### void setStyle(U8 style)
Just a setter for mStyle.\
&nbsp;

#### U8 getStyle() const
Just a getter for mStyle.\
Called in llfontgl.cpp (1).
&nbsp;

#### static std::string getVersionString()
Finding the FT_Library_Version using llformat with major, minor and patch parameters.\
Called in llqppviewer.cpp (1).
&nbsp;

## private :

### --------------------- ATTRIBUTS ---------------------

#### mutable boost::container::flat_map<U64, F32> mKerningCache

#### std::string mName

#### U8 mStyle

#### F32 mPointSize

#### F32 mAscender

#### F32 mDescender

#### F32 mLineHeight

#### LLFT_Face mFTFace

#### BOOL mIsFallback

#### font_vector_t mFallbackFonts
A list of fallback fonts to look for glyphs in (for Unicode chars)
&nbsp;

#### typedef boost::container::flat_map<llwchar, LLFontGlyphInfo*> char_glyph_info_map_t

#### mutable char_glyph_info_map_t mCharGlyphInfoMap
Information about glyph location in bitmap
&nbsp;

#### mutable LLFontBitmapCache* mFontBitmapCachep

#### mutable S32 mRenderGlyphCount

#### mutable S32 mAddGlyphCount

#### 

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### void resetBitmapCache()
Remove every glyphInfo from the memory, clear the map, reset the cache.
&nbsp;

#### void setSubImageLuminanceAlpha(U32 x, U32 y, U32 bitmap_num, U32 width, U32 height, U8 *data, S32 stride = 0) const
Seems to stock some data of the given image inside the main one.
&nbsp;

#### BOOL hasGlyph(llwchar wch) const
Just try to find the llwchar inside the glyphInfoMap.
&nbsp;

#### LLFontGlyphInfo* addGlyph(llwchar wch) const
Add a new character to the font if necessary.
&nbsp;

#### LLFontGlyphInfo* addGlyphFromFont(const LLFontFreetype *fontp, llwchar wch, U32 glyph_index) const
Add a glyph from this font to the other (returns the glyph_index, 0 if not found).
&nbsp;

#### void renderGlyph(U32 glyph_index) const
Calls FT_Load_Glyph and increment the render counter.
&nbsp;

#### void insertGlyphInfo(llwchar wch, LLFontGlyphInfo* gi) const
Deletes the glyphInfos of the given character if it already is in the info map,and replace them by the given infos. If the character didn't exist; creates it, and links the given infos.
&nbsp;

#### bool getKerningCache(U32 left_glyph, U32 right_glyph, F32& kerning) const
Returns true if a kerning has been found, and sets the found kerning in a special place.
&nbsp;

#### void setKerningCache(U32 left_glyph, U32 right_glyph, F32 kerning) const
Memory manipulation to save a new kerning.
&nbsp;