# Location : indra/newview/rendering_engine/llrender

**brief :** LLGL definition. This file contains various stuff for handling gl extensions and other gl related stuff.
# Imports :

- &lt;list&gt; *(from .h file)*
- &lt;boost/unordered_map.hpp&gt; *(from .h file)*
- &lt;glm/mat4x4.hpp&gt; *(from .h file)*
- stdtypes.h *(from .h file)*
- llstring.h *(from .h file)*
- v4math.h *(from .h file)*
- llplane.h *(from .h file)*
- llgltypes.h *(from .h file)*
- llglheaders.h *(from .h file)*
- llglstates.h *(from .h file)*
- linden_common.h
- llsys.h
- llgl.h
- llrender.h
- llerror.h
- llerrorcontrol.h
- llquaternion.h
- llmath.h
- m4math.h
- llstring.h
- llstacktrace.h
- llglheaders.h
- llglslshader.h
- &lt;boost/tokenizer.hpp&gt;
- &lt;glm/vec4.hpp&gt;
- &lt;glm/mat4x4.hpp&gt;
- &lt;glm/gtc/matrix_access.hpp&gt;
- &lt;glm/gtc/matrix_inverse.hpp&gt;
- &lt;glm/gtc/type_ptr.hpp&gt;


# Classes : LLGLManager, LLGLState,LLGLEnableBlending,LLGLEnableAlphaReject,LLGLEnable,LLGLDisable,LLGLUserClipPlane,LLGLSquashToFarClip,LLGLUpdate,LLGLFence,LLGLSyncFence

#### extern BOOL gDebugGL;
#### extern BOOL gDebugSession;
#### extern llofstream gFailLog;
#### extern LLGLManager gGLManager

#### void ll_init_fail_log(const std::string& filename)
Sets up a file for fail logs.\
Called in llappviewer.cpp.
&nbsp;

#### void ll_fail(const std::string& msg)
Some stuff for debug mode, so I don't developp it there.
&nbsp;

#### void ll_close_fail_log()
Just close the fail logs file.
Called in llappviewer.cpp.
&nbsp;

#### void flush_glerror()
Flush GL errors when we know we're handling them correctly. Never used, calls unknown functions, seems really outdated ...
&nbsp;

#### void log_glerror()
This function outputs gl error to the log file, does not crash the code.\
Called in llvertexbuffer.cpp (2).
&nbsp;

#### void assert_glerror()
Some stuff for debug mode, so I don't developp it there.\
Called in llwlparammaanger_test.cpp (1).
&nbsp;

#### void clear_glerror()
Calls unknown function.
Called in llrendertarget.cpp (3).
&nbsp;

#### class LLSD
#### class LLMatrix4


# Class : LLGLManager
Manage GL extensions

## public : 

### --------------------- ATTRIBUTS ---------------------

#### BOOL mInited 
#### BOOL mIsDisabled

#### BOOL mHasMultitexture
#### BOOL mHasATIMemInfo
#### BOOL mHasNVXMemInfo

#### S32 mNumTextureUnits
#### BOOL mHasMipMapGeneration
#### BOOL mHasCompressedTextures
#### BOOL mHasFramebufferObject
#### BOOL mHasFramebufferMultisample
#### BOOL mHasBlendFuncSeparate
    
<span style="color:green">// ARB Extensions</span>

#### BOOL mHasVertexBufferObject
#### BOOL mHasVertexArrayObject
#### BOOL mHasSync
#### BOOL mHasMapBufferRange
#### BOOL mHasFlushBufferRange
#### BOOL mHasPBuffer
#### BOOL mHasShaderObjects
#### S32  mNumTextureImageUnits
#### BOOL mHasOcclusionQuery
#### BOOL mHasTimerQuery
#### BOOL mHasOcclusionQuery2
#### BOOL mHasPointParameters
#### BOOL mHasDrawBuffers
#### BOOL mHasDepthClamp
#### BOOL mHasTransformFeedback
#### S32 mMaxSampleMaskWords
#### S32 mMaxColorTextureSamples
#### S32 mMaxDepthTextureSamples

<span style="color:green">// Other extensions.</span>

#### BOOL mHasAnisotropic
#### BOOL mHasARBEnvCombine
#### BOOL mHasCubeMap
#### BOOL mHasDebugOutput
#### BOOL mHassRGBTexture
#### BOOL mHassRGBFramebuffer
#### BOOL mHasAdaptiveVSync
#### BOOL mHasTextureSwizzle
#### BOOL mHasGpuShader5

<span style="color:green">// Vendor-specific extensions</span>

#### BOOL mIsATI
#### BOOL mIsNVIDIA
#### BOOL mIsIntel
#### BOOL mIsHD3K
#### BOOL mATIOffsetVerticalLines
#### BOOL mATIOldDriver

#### BOOL mHasRequirements
Whether this version of GL is good enough for SL to use
&nbsp;

#### BOOL mHasSeparateSpecularColor
Misc extensions
&nbsp;

#### BOOL mDebugGPU
whether this GPU is in the debug list
&nbsp;

#### S32 mDriverVersionMajor
#### S32 mDriverVersionMinor
#### S32 mDriverVersionRelease
#### F32 mGLVersion
#### S32 mGLSLVersionMajor
#### S32 mGLSLVersionMinor
#### std::string mDriverVersionVendorString
#### std::string mGLVersionString

#### S32 mVRAM
VRAM in MB
&nbsp;

#### S32 mGLMaxVertexRange
#### S32 mGLMaxIndexRange
#### S32 mGLMaxTextureSize
#### S32 mGLMaxVertexUniformComponents

#### std::string mGLVendor
#### std::string mGLVendorShort

#### std::string mGLRenderer


### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### LLGLManager()
Class constructor.
&nbsp;

#### bool initGL()
Returns false if unable (or unwilling due to old drivers) to init GL.\
Called in llwindowmacosx.cpp (1), llwindowmesaheadless.cpp (1).
&nbsp;

#### void shutdownGL()
Just calls glFinish() (Unknown function btw) if one ? is initied.\
Called in llwindowmacosx.cpp (1).
&nbsp;

#### void initWGL()
"Initializes stupid WGL extensions" said a previous author. Actually, seems kinda true. And never called.\
&nbsp;

#### std::string getRawGLString()
For sending to simulator. I don't really get it's utility so far,but it kinda reminds me some url prep for an http request. Hoppfully, it will be more clear it the future .\
Called in llfeaturemanager.cpp (1), llviewerstats.cpp (1).
&nbsp;

#### void getPixelFormat()
Well, no definition of this function ...\
&nbsp;

#### std::string getGLInfoString()
Returns some informations about the GL RENDER/VENDOR/VERSION.\
&nbsp;

#### void printGLInfoString()
A debugging function to print some GL infos\
Called in llappviewer.cpp (1).
&nbsp;

#### void getGLInfo(LLSD& info)
Semms to not really be a getter as it write the VENDOR/RENDER/GL_VERSION at the end of the info table.\
Called in llappviewer.cpp (1).
&nbsp;

## private :

### --------------------- ATTRIBUTS ---------------------

None

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### void initExtensions()
These are used to turn software blending on. They appear in the Debug/Avatar menu presence of vertex skinning/blending or vertex programs will set these to FALSE by default.\
Called in llappviewer.cpp (1).
&nbsp;

#### void initGLStates()
Just calls LLGLState::initClass().\
&nbsp;

#### void initGLImages()
Not defined .\
&nbsp;

#### void setToDebugGPU()
Just some debug function, so I don't go deeper.\
&nbsp;


# Class : LLGLState
LLGLState and its two subclasses, LLGLEnable and LLGLDisable, manage the current 
enable/disable states of the GL to prevent redundant setting of state within a 
render path or the accidental corruption of what state the next path expects.

Essentially, wherever you would call glEnable set a state and then
subsequently reset it by calling glDisable (or vice versa), make an instance of 
LLGLEnable with the state you want to set, and assume it will be restored to its
original state when that instance of LLGLEnable is destroyed.  It is good practice
to exploit stack frame controls for optimal setting/unsetting and readability of 
code.  In llglstates.h, there are a collection of helper classes that define groups
of enables/disables that can cause multiple states to be set with the creation of
one instance.  

A LLGLState initialized with a parameter of 0 does nothing.

LLGLState works by maintaining a map of the current GL states, and ignoring redundant
enables/disables.  If a redundant call is attempted, it becomes a noop, otherwise,
it is set in the constructor and reset in the destructor.  

For debugging GL state corruption, running with debug enabled will trigger asserts
if the existing GL state does not match the expected GL state.

## public : 

### --------------------- ATTRIBUTS ---------------------

None

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### static void initClass()
Activates the map of objects calling GL functions, and disable multisample.\
&nbsp;

#### static void restoreGL()
Just clears the map of objects calling GL functions, and then calls the initclass().\
Called in llviewerwindow.cpp (1).
&nbsp;

#### static void resetTextureStates()
From the last author : "Really shouldn't be needed, but seems we sometimes do"... \
Reset the last texture state .\
&nbsp;

#### static void dumpStates()
Not sure, but seems to just show in the console some infos about the render States.\
&nbsp;

#### static void checkStates(const std::string& msg = "")
A big function used for debugging of the LLGLState class.\
Called in llstartup.cpp (2), llviewerdisplay.cpp (10), llhudtext.cpp (2); llsky.cpp(8), pîpeline/cpp(15).
&nbsp;

#### static void checkTextureChannels(const std::string& msg = "")
Just an other debbuging function.\
Called in llstartup.cpp (2), llviewerdisplay.cpp (~15), llhudtext.cpp (2); llsky.cpp(8), pîpeline/cpp(7).
&nbsp;

#### static void checkClientArrays(const std::string& msg = "", U32 data_mask = 0)
Just an other debbuging function.\
&nbsp;

#### void setEnabled(S32 enabled)
Allows to activate or desactivate GL work of a certain state if not the current one.\
Called in lllocalcliprect.cpp (1).
&nbsp;

#### void enable() { setEnabled(TRUE); }
Return the 'position' (not sure) of the glyph parameter.\
Called in llfontgl.cpp (2).
&nbsp;


## Private :

### --------------------- ATTRIBUTS ---------------------

None

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

None

## Protected :

### --------------------- ATTRIBUTS ---------------------

static boost::unordered_map<LLGLenum, LLGLboolean> sStateMap;

LLGLenum mState;
BOOL mWasEnabled;
BOOL mIsEnabled;

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

None


# Class : LLGLUserClipPlane
Store and modify projection matrix to create an oblique
projection that clips to the specified plane.  Oblique
projections alter values in the depth buffer, so this
class should not be used mid-renderpass.  

Restores projection matrix on destruction.
GL_MODELVIEW_MATRIX is active whenever program execution
leaves this class.
Does not stack.
Caches inverse of projection matrix used in gGLObliqueProjectionInverse
## public : 

### --------------------- ATTRIBUTS ---------------------

None

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### <span style="color:darkorange">Constructor </span>LLGLUserClipPlane(const LLPlane& plane, const glm::mat4& modelview, const glm::mat4& projection, bool apply = true)
Just sets the parameters as objects parameters and calls setPlane().\
Called in pipeline.cpp(4).
&nbsp;

#### void setPlane(F32 a, F32 b, F32 c, F32 d)
Manipulates planes and matrices to create an oblique projection . However, why putting it public ??? \
&nbsp;

## private : 

### --------------------- ATTRIBUTS ---------------------

bool mApply;

glm::mat4 mProjection;  //unknown ,maybe cause from Opengl
glm::mat4 mModelview;

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

None


# Class : LLGLSquashToFarClip
Modify and load projection matrix to push depth values to far clip plane.

Restores projection matrix on destruction.
GL_MODELVIEW_MATRIX is active whenever program execution
leaves this class.
Does not stack.
## public : 

### --------------------- ATTRIBUTS ---------------------

None

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### <span style="color:darkorange">Constructor </span>LLGLSquashToFarClip(glm::mat4 projection, U32 layer = 0)
Applies some modifications to the Projection and Modelview matricies
&nbsp;



# Class : LLGLUpdate
Interface for objects that need periodic GL updates applied to them.
Used to synchronize GL updates with GL thread.
## public : 

### --------------------- ATTRIBUTS ---------------------

static std::list<LLGLUpdate*> sGLQ;
BOOL mInQ;

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### <span style="color:darkorange">Constructor </span>LLGLUpdate(): mInQ(FALSE)
TBH, I don't really understand this one yet :(
Called in llviewerparceloverlay.h (1), llviewerobject.h(1); pipeline.cpp(6),pipeline.h(1).
&nbsp;

