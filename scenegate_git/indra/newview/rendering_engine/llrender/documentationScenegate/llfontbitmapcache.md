
# Location : indra/newview/rendering_engine/llrender

**brief :** Storage for previously rendered glyphs.Maintain a collection of bitmaps containing rendered glyphs. Generalizes the single-bitmap logic from LLFontFreetype and LLFontGL.

*ndl*: a **glyph** is a character or a little object in a big sheet containing lots of similar objects inside which we pick what we need. For exemple, to write words, we'll use a sheet containing all the alphabet, where every letter wanna be a glyph.

# Imports :

- linden_common.h
- llfontbitmapcache.h
- llgl.h
- lltrace.h *(from .h file)*`

# Classes : LLFontBitmapCache

## public : 

### --------------------- ATTRIBUTS ---------------------

None 

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### LLFontBitmapCache()
Constructor, just initialize several variables.\
Called in sllfontfreetype.cpp (2), llfontgl.cpp (1).
&nbsp;
#### void init(S32 num_components, S32 max_char_width, S32 max_char_height)
*Need to call this once, before caching any glyphs*. Calls reset(), and links the function's parameters to the class parameters.\
Called in llfontfreetype.cpp (1).
&nbsp;
#### void reset()
Remove memory claimed and reset the mImageRawVect and the mImageGLVect, reset mBitmap and mOffset.\
Called in llfontfreetype.cpp (1).
&nbsp;
#### BOOL nextOpenPos(S32 width, S32 &posX, S32 &posY, S32 &bitmapNum)
Browse in the image rows, and set up a new/next one if needed.\
Called in llfontfreetype.cpp (1).
&nbsp;
#### void destroyGL()
Destroy the texture stored in the image.\
Called in llfontfreetype.cpp (1).
&nbsp;
#### LLImageRaw *getImageRaw(U32 bitmapNum = 0) const
Just a getter for the imageRaw pointer.\
Called in llfontfreetype.cpp (3).
&nbsp;
#### LLImageGL *getImageGL(U32 bitmapNum = 0) const
Just a getter for the imageGLVect pointer.\
Called in llfontfreetype.cpp (1), llfontgl.cpp (1).
&nbsp;
#### S32 getMaxCharWidth() const
&nbsp;
#### S32 getNumComponents() const 
&nbsp;
#### S32 getBitmapWidth() const
Called in llfontgl.cpp (1).
&nbsp;
#### S32 getBitmapHeight() const
Called in llfontgl.cpp (1).
&nbsp;

## private :

### --------------------- ATTRIBUTS ---------------------

#### S32 mNumComponents
&nbsp;
#### S32 mBitmapWidth
&nbsp;
#### S32 mBitmapHeight
&nbsp;
#### S32 mBitmapNum
&nbsp;
#### S32 mMaxCharWidth
&nbsp;
#### S32 mMaxCharHeight
&nbsp;
#### S32 mCurrentOffsetX
&nbsp;
#### S32 mCurrentOffsetY
&nbsp;
#### std::vector<LLPointer<LLImageRaw> >	mImageRawVec
&nbsp;
#### std::vector<LLPointer<LLImageGL> > mImageGLVec
&nbsp;

### --------------------- CLASSES ---------------------

None

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

None