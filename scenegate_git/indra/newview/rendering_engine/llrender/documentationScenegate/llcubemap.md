
# Location : indra/newview/rendering_engine/llrender

*ndl* : mipmaps are pre-calculated, optimized sequences of images, each of which is a progressively lower resolution representation of the previous.They are intended to increase rendering speed and reduce aliasing artifacts. Since mipmaps, by definition, are pre-allocated, additional storage space is required to take advantage of them. They are also related to wavelet compression. Mipmap textures are used in 3D scenes to decrease the time required to render a scene. They also improve image quality by reducing aliasing and Moiré patterns that occur at large viewing distances,at the cost of 33% more memory per texture. (*Wikipedia*)


# Imports :

- class LLVector3;
- linden_common.h
- llcubemap.h
- llgl.h *(from .h file)*
- llglheaders.h
- llglslshader.h
- llrender.h
- llworkerthread.h
- m3math.h
- m4math.h
- v3dmath.h
- v3math.h
- v4coloru.h

# Classes : LLCubeMap

## public : 

### --------------------- ATTRIBUTS ---------------------

#### static bool sUseCubeMaps 

In opposition to "use_cube_mipmaps". As the previous authors said : "current build works best without cube mipmaps", sUseCubeMaps is set to "TRUE" and use_cube_mipmaps to "FALSE" at the begining of the script. 

**appels exterieurs :**

- **llappviewer.cpp** (1) : LLCubeMap::sUseCubeMaps = LLFeatureManager::getInstance()->isFeatureAvailable("RenderCubeMap")
- **llfloaterpreference.cpp** (3) : needed seted to "TRUE" to set the booleans "bumpshiny" and  "reflexions" to "TRUE".
- **llrender.cpp (1)** : needed to set up glBindTexture and other stuff
- **lldrawpoolwater.cpp** (1) :
- **llvosky.cpp** (2): 



### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

#### LLCubeMap()
Constructor, sets up the mTargets[0-5] to  GL_TEXTURE_CUBE_MAP_ negative or positive, and on X,Y or Z.
Also call mTextureStage(0), mTextureCoordStage(0), mMatrixStage(0).
&nbsp;
#### void init(const std::vector<LLPointer<LLImageRaw> >& rawimages)
Launches initGL(), initRawData(rawimages) and initGLData().\
Called in llvosky.cpp (4).
&nbsp;
#### void initGL()
Seems to be a generic function used in other classes. Initialize mImages with textures and other stuff if not already done.
&nbsp;
#### void initRawData(const std::vector<LLPointer<LLImageRaw> >& rawimages)
Weird piece of code to transpose and write piece after piece rawimages buffers in mRawImages buffers.
&nbsp;
#### void initGLData()
Set up every subImage with the mRawImages.
&nbsp;
#### void bind()
Bind the cubeMap object on the llTextUnit.\
Called in lldrawpoolwater.cpp (1) and pipieline.cpp (1)
&nbsp;
#### void enable(S32 stage)
Just calls **enableTexture()** and **enableTextureCoords()**.\
Called in lldrawpoolbump.cpp (2), lldrawpoolwater.cpp (1) and pipeline.cpp (1).
&nbsp;
#### void disable(void)
Just calls disableTexture() and disableTextureCoords().\
Called in lldrawpoolbump.cpp (2), lldrawpoolwater.cpp (1) and pipeline.cpp (1).
&nbsp;
#### void enableTexture(S32 stage)
As said, enable the gGl *LLTexUnit::TT_CUBE_MAP* object if sUseCubeMaps==TRUE.\
Called in lldrawpoolbump.cpp (2).
&nbsp;
#### void enableTextureCoords(S32 stage)
Weird piece of code *(not well written btw)* that activates some glEnable() and glTexGeni(), the definition of which I cannot find .\ 
Called in lldrawpoolbump.cpp (2).
&nbsp;
#### void disableTexture(void) 
Disable the gGl TexUnit of the mTextureStage.
&nbsp;
#### void LLCubeMap::disableTextureCoords(void)
Call unknown function glDisable several times.
&nbsp;
#### void setMatrix(S32 stage)
Set up and load matrice calling gGl functions.\
Called in lldrawpoolbump.cpp (4).
&nbsp;
#### void restoreMatrix()
Reset the gGl MatrixMode to default
&nbsp;
#### void setReflection (void)
Sets several textures options for the textures of the cubeMap.
&nbsp;
#### GLuint getGLName() const
Just returns the cubeMap name.\
&nbsp;
#### LLVector3 map(U8 side, U16 v_val, U16 h_val) const
Update the map normalized vector, used to build a ray .\
&nbsp;
#### BOOL project(F32& v_val, F32& h_val, BOOL& outside, U8 side, const LLVector3& dir) const
Seems to test something on the cubemap position or direction, or perhaps if it's seen.
&nbsp;
#### BOOL project(F32& v_min, F32& v_max, F32& h_min, F32& h_max, U8 side, LLVector3 dir[4]) const
Uses the other project function to determine if the cubeMap is or isn't inside something that I haven't find yet, perhaps the field of vision
&nbsp;
#### void paintIn(LLVector3 dir[4], const LLColor4U& col)
Seems to update the color of the images of the cubeMap depending on the (field of view) ray. But never used :/
&nbsp;
#### void destroyGL()
Restets all the mImages.\
Called in llvosky.cp
&nbsp;

## protected :

### --------------------- ATTRIBUTS ---------------------

#### LLGLenum mTargets[6]
Stocks the cubeMap GL textures codes for pos & neg (X,Y,Z)
&nbsp;
#### LLPointer<LLImageGL> mImages[6]
Stocks many infos about the images of the cubeMap.\
Used in llrender.cpp (9).
&nbsp;
#### LLPointer<LLImageRaw> mRawImages[6]
Local buffers to store and transfer data images.\
&nbsp;
#### S32 mTextureStage ,S32 mTextureCoordStage, S32 mMatrixStage
Just a local variables to store the stage related stuff.\
&nbsp;

### --------------------- CLASSES ---------------------

friend class LLTexUnit

### --------------------- STRUCTURES ---------------------

None

### ---------------------  METHODES  ---------------------

None