# Location : indra/newview/rendering_engine/llrender

**brief :**  LLRender definition

# Imports :

- &lt;glm/mat4x4.hpp&gt; *(from .h file)*
- &lt;boost/align/aligned_allocator.hpp&gt; *(from .h file)*
- v2math.h *(from .h file)*
- v3math.h *(from .h file)*
- v4math.h *(from .h file)*
- v4coloru.h *(from .h file)*
- llstrider.h *(from .h file)*
- llpointer.h *(from .h file)*
- llglheaders.h *(from .h file)*
- llmatrix4a.h *(from .h file)*
- llvector4a.h *(from .h file)*
- linden_common.h
- llrender.h
- llvertexbuffer.h
- llcubemap.h
- llglslshader.h
- llimagegl.h
- llrendertarget.h
- lltexture.h
- llshadermgr.h
- &lt;glm/vec3.hpp&gt;
- &lt;glm/vec4.hpp&gt;
- &lt;glm/mat4x4.hpp&gt;
- &lt;glm/gtc/matrix_inverse.hpp&gt;
- &lt;glm/gtc/matrix_transform.hpp&gt;
- &lt;glm/gtc/type_ptr.hpp&gt;

<span style="color:blue">class</span> LLVertexBuffer;
<span style="color:blue">class</span> LLCubeMap;
<span style="color:blue">class</span> LLImageGL;
<span style="color:blue">class</span> LLRenderTarget;
<span style="color:blue">class</span> LLTexture ;

<span style="color:darkgreen">extern</span> F32 gGLModelView[16];
<span style="color:darkgreen">extern</span> F32 gGLLastModelView[16];
<span style="color:darkgreen">extern</span> F32 gGLLastProjection[16];
<span style="color:darkgreen">extern</span> F32 gGLProjection[16];
<span style="color:darkgreen">extern</span> S32 gGLViewport[4];

<span style="color:darkgreen">extern</span> LLRender gGL;



# Classes : LLTexUnit, LLGLStLLLightState, LLRender



# Class : LLTexUnit
Manage GL extensions

## public : 

### --------------------- ATTRIBUTS ---------------------

#### static U32 sWhiteTexture;


### --------------------- STRUCTURES/ENUM ---------------------

#### typedef enum eTextureType:
	
TT_TEXTURE = 0,	<span style="color:green">// Standard 2D Texture</span>
TT_CUBE_MAP, <span style="color:green">// 6-sided cube map texture</span>
TT_NONE  <span style="color:green">// No texture type is currently enabled</span>
&nbsp;

#### typedef enum eTextureAddressMode:
	
TAM_WRAP = 0, <span style="color:green">// Standard 2D Texture</span>
TAM_MIRROR, <span style="color:green">// Non power of 2 texture</span>
TAM_CLAMP  <span style="color:green">// No texture type is currently enabled</span>
&nbsp;

#### typedef enum eTextureFilterOptions:
<span style="color:green">// Note: If mipmapping or anisotropic are not enabled or supported it should fall back gracefully	</span>
TFO_POINT = 0, <span style="color:green">// Equal to: min=point, mag=point, mip=none.</span>
TFO_BILINEAR, <span style="color:green">// Equal to: min=linear, mag=linear, mip=point.</span>
TFO_TRILINEAR,  <span style="color:green">// Equal to: min=linear, mag=linear, mip=linear.</span>
TFO_ANISOTROPIC  <span style="color:green">// Equal to: min=anisotropic, max=anisotropic, mip=linear.</span>
&nbsp;

#### typedef enum eTextureBlendType:
	
TB_REPLACE = 0, 
TB_ADD, 
TB_MULT,  
TB_MULT_X2,
TB_ALPHA_BLEND,
TB_COMBINE <span style="color:green">// Doesn't need to be set directly, setTexture___Blend() set TB_COMBINE automatically</span>
&nbsp;

#### typedef enum eTextureBlendOp:
	
TBO_REPLACE = 0, <span style="color:green">// Use Source 1</span>
TBO_MULT, <span style="color:green">// Multiply: ( Source1 * Source2 )</span>
TBO_MULT_X2,  <span style="color:green">// Multiply then scale by 2:  ( 2.0 * ( Source1 * Source2 ) )</span>
TBO_MULT_X4, <span style="color:green">// Multiply then scale by 4:  ( 4.0 * ( Source1 * Source2 ) )</span>
TBO_ADD, <span style="color:green">// Add: ( Source1 + Source2 )</span>
TBO_ADD_SIGNED, <span style="color:green">// Add then subtract 0.5: ( ( Source1 + Source2 ) - 0.5 )</span>
TBO_SUBTRACT, <span style="color:green">// Subtract Source2 from Source1: ( Source1 - Source2 )</span>
TBO_LERP_VERT_ALPHA, <span style="color:green">// Interpolate based on Vertex Alpha (VA): ( Source1 * VA + Source2 * (1-VA) )</span>
TBO_LERP_TEX_ALPHA, <span style="color:green">// Interpolate based on Texture Alpha (TA): ( Source1 * TA + Source2 * (1-TA) )</span>
TBO_LERP_PREV_ALPHA, <span style="color:green">// Interpolate based on Previous Alpha (PA): ( Source1 * PA + Source2 * (1-PA) )</span>
TBO_LERP_CONST_ALPHA, <span style="color:green">// Interpolate based on Const Alpha (CA): ( Source1 * CA + Source2 * (1-CA) )</span>
TBO_LERP_VERT_COLOR<span style="color:green">// Interpolate based on Vertex Col (VC): ( Source1 * VC + Source2 * (1-VC) )</span>
&nbsp;
<span style="color:green">// *Note* TBO_LERP_VERTEX_COLOR only works with setTextureColorBlend(), and falls back to TBO_LERP_VERTEX_ALPHA for setTextureAlphaBlend()</span>

&nbsp;

#### typedef enum eTextureBlendSrc:
	
TBS_PREV_COLOR = 0, <span style="color:green">// Color from the previous texture stage</span>
TBS_PREV_ALPHA,
TBS_ONE_MINUS_PREV_COLOR, 
TBS_ONE_MINUS_PREV_ALPHA, 
TBS_TEX_COLOR, <span style="color:green">// Color from the texture bound to this stage</span>
TBS_TEX_ALPHA, 
TBS_ONE_MINUS_TEX_COLOR,
TBS_ONE_MINUS_TEX_ALPHA, 
TBS_VERT_COLOR, <span style="color:green">// The vertex color currently set</span>
TBS_VERT_ALPHA, 
TBS_ONE_MINUS_VERT_COLOR, 
TBS_ONE_MINUS_VERT_ALPHA,
TBS_CONST_COLOR,<span style="color:green">// The constant color value currently set</span>
TBS_CONST_ALPHA,
TBS_ONE_MINUS_CONST_COLOR,
TBS_ONE_MINUS_CONST_ALPHA

&nbsp;

### ---------------------  METHODES  ---------------------

#### <span style="color:darkorange">Constructor </span> LLTexUnit(S32 index)
Just sets up many initials parameters.
&nbsp;

#### void refreshState(void)
Refreshes renderer state of the texture unit to the cached values. Needed when the render context has changed and invalidated the current state.\
Called in 95 differents files so I won't write them all there :/.
&nbsp;