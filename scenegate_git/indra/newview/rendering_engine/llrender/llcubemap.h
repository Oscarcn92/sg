/** 
 * @file llcubemap.h
 * @brief LLCubeMap class definition
 *
 * $LicenseInfo:firstyear=2002&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 * 
 * new comments : GM , 08/21
 */

#ifndef LL_LLCUBEMAP_H
#define LL_LLCUBEMAP_H

#include "llgl.h"

#include <vector>

class LLVector3;

// Environment map hack!
class LLCubeMap : public LLRefCount
{
public:
	/*Constructor, sets up the mTargets[0-5] to  GL_TEXTURE_CUBE_MAP_ negative or positive, and on X,Y or Z.
	Also call mTextureStage(0), mTextureCoordStage(0), mMatrixStage(0).*/
	LLCubeMap();

	/*launches initGL(), initRawData(rawimages) and initGLData()*/
	void init(const std::vector<LLPointer<LLImageRaw> >& rawimages);
	
	/*seems to be a generic function used in other classes. Initialize mImages with textures and other stuff if not already done*/
	void initGL();

	/*Weird piece of code to transpose and write piece after piece rawimages buffers in mRawImages buffers */
	void initRawData(const std::vector<LLPointer<LLImageRaw> >& rawimages);
	
	/*set up every subImage with the mRawImages*/
	void initGLData();

	/*bind the cubeMap object on the llTextUnit*/
	void bind();

	/*just launches enableTexture() and enableTextureCoords*/
	void enable(S32 stage);
	
	/*As said, enable the gGl LLTexUnit::TT_CUBE_MAP object if sUseCubeMaps==TRUE.*/
	void enableTexture(S32 stage);
	
	/*Weird piece of code *(not well written btw)* that activates some glEnable() and glTexGeni(), the definition of which I cannot find .*/
	void enableTextureCoords(S32 stage);

	/*never called ...*/
	S32	 getStage(void) const { return mTextureStage; } 
	
	/*Just calls disableTexture() and disableTextureCoords()*/
	void disable(void);

	/*Disable the gGl TexUnit of the mTextureStage*/
	void disableTexture(void);

	/*Call unknown function glDisable several times*/
	void disableTextureCoords(void);

	/*Set up and load matrice calling gGl functions*/
	void setMatrix(S32 stage);
	
	/*Reset the gGl MatrixMode to default*/
	void restoreMatrix();

	/*Sets several textures options for the textures of the cubeMap*/
	void setReflection (void);

	/*never defined or called...*/
	void finishPaint();

	/*Just returns the cubeMap name*/
	GLuint getGLName() const;

	/*Update the map normalized vector, used to build a ray*/
	LLVector3 map(U8 side, U16 v_val, U16 h_val) const;

	/*Seems to test something on the cubemap position or direction, or perhaps if it's seen.*/
	BOOL project(F32& v_val, F32& h_val, BOOL& outside,
						U8 side, const LLVector3& dir) const;

	/*Uses the other project function to determine if the cubeMap is or isn't inside something that I haven't find yet*/					
	BOOL project(F32& v_min, F32& v_max, F32& h_min, F32& h_max, 
						U8 side, LLVector3 dir[4]) const;

	/*Seems to update the color of the images of the cubeMap depending on the (field of view) ray. But never used :/*/					
	void paintIn(LLVector3 dir[4], const LLColor4U& col);

	/*Restets all the mImages*/
	void destroyGL();

public:
	/* In opposition to "use_cube_mipmaps". As the previous authors said : "current build works best without cube mipmaps", sUseCubeMaps is set to "TRUE" and use_cube_mipmaps to "FALSE" at the begining of the script. 
	
	appels exterieurs :
	- llappviewer.cpp (1) : LLCubeMap::sUseCubeMaps = LLFeatureManager::getInstance()->isFeatureAvailable("RenderCubeMap")
	- llfloaterpreference.cpp (3) : needed seted to "TRUE" to set the booleans "bumpshiny" and  "reflexions" to "TRUE".
	- llrender.cpp (1 : needed to set up glBindTexture and other stuff
	- lldrawpoolwater.cpp (1) :
	- llvosky.cpp** (2): */
	static bool sUseCubeMaps;

protected:
	friend class LLTexUnit;
	~LLCubeMap();

	/*Stocks the cubeMap GL textures codes for pos & neg (X,Y,Z)*/
	LLGLenum mTargets[6];

	/*Stocks many infos about the images of the cubeMap*/
	LLPointer<LLImageGL> mImages[6];

	/*Local buffers to store and transfer data images*/
	LLPointer<LLImageRaw> mRawImages[6];

	
	S32 mTextureStage;
	S32 mTextureCoordStage;
	S32 mMatrixStage;
};

#endif
