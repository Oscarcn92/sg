 /** 
 * @file objectToUpdate.cpp
 * @brief objectToUpdate class implementation
 *
 * 
 */

#include "objectToUpdate.h"
#include "lldrawable.h"
#include "llface.h"
#include "llvovolume.h"

ObjectToUpdate::ObjectToUpdate( LLPointer<LLViewerObject> viewerObject, UpdateType _updateType){
    
    this->updateType = _updateType;
    this->name = viewerObject->mID.getString();
    this->viewerObject = viewerObject;
    
    if(_updateType==UpdateType::GEOMETRY){
        LL_WARNS() << "OK3" << LL_ENDL;
        this->setGeometry();

    }

    // Update for textures, not yet functionnal
    if(_updateType==UpdateType::TEXTURE){
        this->setTexture(viewerObject);

    }

    //By default
    this->objType = objType::OTHER;

    LLVector3 pos = viewerObject->getPositionEdit();
    this->position[0]=pos.mV[0];
    this->position[1]=pos.mV[1];
    this->position[2]=pos.mV[2];

    if(viewerObject->isAttachment()){
        this->objType = objType::ATTACHEMENT;
    }
    else
    {
        std::vector<LLViewerObject*> objects;
        //To check if flexible, need to also check if child or parent of the viewerObject is flexible
        viewerObject->getRootEdit()->addThisAndAllChildren(objects);
        for (LLViewerObject* obj : objects){
            if(obj->isFlexible()){
                this->objType = objType::FLEXIBLE;
                LLVector3 pos = viewerObject->getRootEdit()->getPositionEdit();
                LLQuaternion quat =viewerObject->getRootEdit()->getRotationEdit();

                std::vector<float>position={0,0,0};
                
                position[0]=pos.mV[0];
                position[1]=pos.mV[1];
                position[2]=pos.mV[2];

                //flex objects meshes are not automatically translated when we pick them up from the buffer,(still unable to perfectly figure out why),
                //so we do the translation/rotations here.
                this->rotate(quat);
                this->translate(position);
                
                break;
            }
            
        }
    }
    


}

void ObjectToUpdate::translate(std::vector<float> translation){
    for (Face  &face : this->faces){
        
        for (int i = 0; i<face.verticles.size(); i+=3) {
            face.verticles[i]+=translation[0];
            face.verticles[i+1]+=translation[1];
            face.verticles[i+2]+=translation[2];
        }
    }
}



void ObjectToUpdate::rotate(LLQuaternion quat){
    // for (Face  &face : this->faces){
        

    //     for (int i = 0; i<face.verticles.size(); i+=3) {

    //         LLVector3 pos = LLVector3(face.verticles[i],face.verticles[i+1],face.verticles[i+2]);
    //         pos= pos.rotVec(quat);
    //         pos=pos.scaledVec(viewerObject/*->getRootEdit()*/->getScale());


    //         face.verticles[i]+=pos.mV[0];
    //         face.verticles[i+1]+=pos.mV[1];
    //         face.verticles[i+2]+=pos.mV[2];
    //     }
    // }
}



//Geometry took in faces of LLDrawable, so traingles are already at their final position inside the region
void ObjectToUpdate::setGeometry()
{
    
    LLPointer<LLDrawable> mDrawable = this->viewerObject->mDrawable; 
    this->nbreFaces = mDrawable->getNumFaces();

    for (S32 i = 0; i < mDrawable->getNumFaces(); ++i)
    {
        Face faceStruct;
        
        LLFace* face = mDrawable->getFace(i);

        LLStrider<LLVector3> verticesp;
        LLStrider<LLVector3> normalsp;
        LLStrider<U16> indicesp;
        LLStrider<LLVector2> tex_coordsp;
    
        face->getGeometry(verticesp, normalsp,tex_coordsp,  indicesp);

        if(!face->isBufferNull())
        {
            LLPointer<LLVertexBuffer> buffer = face->getVertexBuffer();
            bool hasNormal =buffer->hasDataType(LLVertexBuffer::TYPE_NORMAL);


            for (U32 j = 0; j < face->getGeomCount(); ++j)
            {
                LLVector3 pos = LLVector3(verticesp[j]);
                faceStruct.verticles.push_back(pos.mV[0]);
                faceStruct.verticles.push_back(pos.mV[1]);
                faceStruct.verticles.push_back(pos.mV[2]);

                //Sometimes no normal, so requires a special treatment
                LLVector3 normal = * new LLVector3();
                if(hasNormal){
                    normal = LLVector3(normalsp[j]);
                }

                faceStruct.normals.push_back(normal.mV[0]);
                faceStruct.normals.push_back(normal.mV[1]);
                faceStruct.normals.push_back(normal.mV[2]);
            }
            for (S32 j = 0; j < face->getIndicesCount(); ++j)
            {
                U16 idx = indicesp[j] -face->getGeomIndex();
                faceStruct.indices.push_back(idx);
            }
            this->faces.push_back(faceStruct);   
        }

        else
        {     
            this->nbreFaces-=1;
        }

    }
}

// Function still in need on work for all texture types, works for most objects by extracting the raw data
void ObjectToUpdate::setTexture(LLPointer<LLViewerObject> viewerObject) {

    TextureFace textureStruct;
    
    LLPointer<LLDrawable> mDrawable = viewerObject->mDrawable;
    
    this->nbreFaces = mDrawable->getNumFaces();
    // LL_WARNS() << "OK4" << LL_ENDL;

    for (S32 i = 0; i < mDrawable->getNumFaces(); ++i)
    {
        // LL_WARNS() << "OK4.1" << LL_ENDL;
        LLTextureEntry *TE = viewerObject->getTE(i);
        // LL_WARNS() << "OK4.1.5" << LL_ENDL;
        if (TE) {
            const LLUUID& TE_id = TE->getID();
            // LL_WARNS() << "OK4.2" << LL_ENDL;
            LLViewerFetchedTexture *FetchedImage = LLViewerTextureManager::getFetchedTexture(TE_id, 
            FTT_DEFAULT, TRUE, LLGLTexture::BOOST_NONE, LLViewerTexture::LOD_TEXTURE, 0, 0, LLHost());
            // LL_WARNS() << "OK4.3" << LL_ENDL;
            LLImageRaw *imageRAW;
            // LL_WARNS() << "OK5" << LL_ENDL;

            if (FetchedImage->isCachedRawImageReady()) {
                imageRAW = FetchedImage->getCachedRawImage();
                // LL_WARNS() << "GOT CACHED" << LL_ENDL;
            } else if (FetchedImage->hasSavedRawImage()){
                imageRAW = FetchedImage->getSavedRawImage();
                // LL_WARNS() << "GOT SAVED" << LL_ENDL;
            } else {
                FetchedImage->forceToSaveRawImage();
                imageRAW = FetchedImage->getSavedRawImage();
                // LL_WARNS() << "GOT SAVED after SAVING" << LL_ENDL;
            }
            // LL_WARNS() << "OK6" << LL_ENDL;
            if (imageRAW)
            {
                U8* imageData = imageRAW->getData(); 
                U16 imageWidth = imageRAW->getWidth(); 
                U16 imageHeight = imageRAW->getHeight(); 
                S32 imageComponent = imageRAW->getComponents();
                S32 imageDataSize = imageRAW->getDataSize();

                // LL_WARNS() << "OK7" << LL_ENDL;
                for (S32 k = 0; k < imageDataSize; k++)
                {
                    textureStruct.data.push_back(*(imageData+k));
                }

                textureStruct.width = imageWidth;
                textureStruct.height = imageHeight;
                textureStruct.component = imageComponent;
                textureStruct.dataSize = imageDataSize;

                this->TexFaces.push_back(textureStruct);
            }
        }
    }

}

void ObjectToUpdate::convertToText()
{   
    int i =0;
    for (Face const& face : this->faces){
        
        std::vector<std::string> strV;
        std::vector<std::string> strN;
        std::vector<std::string> strI;

        for (float v: face.verticles) {
            strV.push_back(std::to_string(v));
        }

        for (float n: face.normals) {
            strN.push_back(std::to_string(n));
        }

        for (float i: face.indices) {
            strI.push_back(std::to_string(i));
        }


        std::ofstream output_file1("./Meshes/positions"+this->name+std::to_string(i)+".txt");
        std::ostream_iterator<std::string> output_iterator1(output_file1, "\n");
        std::copy(strV.begin(), strV.end(), output_iterator1);
        output_file1.close();
        
        std::ofstream output_file2("./Meshes/normals"+this->name+std::to_string(i)+".txt");
        std::ostream_iterator<std::string> output_iterator2(output_file2, "\n");
        std::copy(strN.begin(), strN.end(), output_iterator2);
        output_file2.close();

        std::ofstream output_file3("./Meshes/indices"+this->name+std::to_string(i)+".txt");
        std::ostream_iterator<std::string> output_iterator3(output_file3, "\n");
        std::copy(strI.begin(), strI.end(), output_iterator3);
        output_file3.close();

        i++;
    }

    //header now :
    std::vector<std::string> datInfo;
    datInfo.push_back(std::to_string(this->nbreFaces));

    std::ofstream output_file4("./Meshes/header"+this->name+".txt");
    std::ostream_iterator<std::string> output_iterator4(output_file4, "\n");
    std::copy(datInfo.begin(), datInfo.end(), output_iterator4);
    output_file4.close();
}

