/** 
 * @file renderAPI.cpp
 * @brief renderAPI class implementation
 *
 * 
*/

#include "renderAPI.h"
#include "llviewerobjectlist.h"
#include "xform.h"
#include "llviewerregion.h"
#include "lldrawable.h"
#include "llvoavatar.h"
#include "apiCameraChanges.hpp"
#include <boost/filesystem.hpp>

extern ApiCAMCHANGE livecamchange;
extern LLViewerObjectList gObjectList;
RenderAPI renderAPI;

int increment = 0;

RenderAPI::RenderAPI()
{
    //For test only


    
    boost::filesystem::path dir("./Meshes");
    boost::filesystem::create_directory(dir);
    

    std::ofstream file("./askForUpdate.txt");
    file << "ENDWRITE";
    file.close();
    
}


//Called every frame
void RenderAPI::updateUpdateList()
{
    increment++;
    std::fstream output_file;
	/////////////// Only for tests 	//////////////////////////////////////////////
    bool test=false;
    
    std::string a;
    output_file.open("./askForUpdate.txt",  std::ios_base::in);
    while (getline(output_file, a)){
        
        if(a=="isAsking"){
            test=true;
        }
    }
    output_file.close();
    
    if(test){
        
        getUpdateList();        
        output_file.open("./askForUpdate.txt", std::ios_base::out |std::ios_base::trunc);
        output_file << "ENDWRITE";
        output_file.close();
    }
    /////////////////////////////////////// END Only for tests /////////////////////


    //protection multithreading
    //TODO ameliorate that
    while (!this->tokenUpdateListAvailable)
    {
        /* code */
    }
    this->tokenUpdateListAvailable=false;

    //Save objects that have been updated since last call of godot
    LL_WARNS() << "BP!" << LL_ENDL;
    LLViewerObject* pObject = nullptr;

    U32 objCnt = gObjectList.getNumObjects();

    int objChangedThisFrame = 0;
    int objDeletedThisFrame = 0;

    
    for (int i= 0; i<objCnt;i++)
	{

        pObject = gObjectList.getObjectAliveOrDead( i );

        if (pObject){

            if (pObject->isDead())
            {
                
                this->objKill.insert(pObject);
                this->objUpdate.erase(pObject);
                objDeletedThisFrame++;
            }
            else
            {
                LLDrawable *pDrawable = pObject->mDrawable;
                if(pDrawable)
                {
                    //For now, only render one region at a time
                    if(pObject->getRegion()==nullptr){
                    }
                    else if(pObject->getRegion()->getName()=="TVTC_2" )
                    {
                        // attachements not working perfectly 
                        if(!pObject->isAttachment())
                        {
                            if (pDrawable->isState(LLDrawable::REBUILD_ALL)  )
                            {
                                
                                this->objUpdate.insert(pObject);
                                this->objKill.erase(pObject);
                                objChangedThisFrame++;
                                // LL_WARNS() << "objUpdate size : " << objUpdate.size() << LL_ENDL;
                            }
                            // this section is for testing texture changes

                            // if (pDrawable->isState(LLDrawable::REBUILD_MATERIAL))
                            // {
                            //     this->objUpdateTexture.insert(pObject);
                            // }
                        }

                    }
                }
            }
        }
	}

    //LL_WARNS() << "objects update this frame :"<<objChangedThisFrame << LL_ENDL;
    //LL_WARNS() << "objects deleted this frame :"<<objDeletedThisFrame << LL_ENDL;
    this->tokenUpdateListAvailable=true;
}


std::list<ObjectToUpdate> RenderAPI::getUpdateList()
{

    while (!this->tokenUpdateListAvailable)
    {
        /* code */
    }
    this->tokenUpdateListAvailable=false;

    std::list<ObjectToUpdate> objectsToUpdate ;

    for (LLPointer<LLViewerObject> const& obj : this->objKill){
  
        ObjectToUpdate objToUpdate = ObjectToUpdate(obj, ObjectToUpdate::UpdateType::DESTRUCTION);
        objectsToUpdate.push_back(objToUpdate);
    }

    for (LLPointer<LLViewerObject> const& obj : this->objUpdate){
        if(obj->mDrawable){
            
            ObjectToUpdate objToUpdate = ObjectToUpdate(obj, ObjectToUpdate::UpdateType::GEOMETRY);
            objectsToUpdate.push_back(objToUpdate);
            
        }
    }

    // Follow up code to test textures, creates call to ObjectToUpdate with type TEXTURE

    // for (LLPointer<LLViewerObject> const& obj : this->objUpdateTexture){
    //     if(obj->mDrawable){
    //         ObjectToUpdate objToUpdate = ObjectToUpdate(obj, ObjectToUpdate::UpdateType::TEXTURE);
    //         objectsToUpdate.push_back(objToUpdate);
            
    //     }
    // }


    //Clear the sets after they've been looked
    this->objKill.clear();
    this->objUpdate.clear();
    this->objUpdateTexture.clear();

    //ONly for tests
    convertListToText(objectsToUpdate);

    this->tokenUpdateListAvailable=true;
    return objectsToUpdate;
}

void RenderAPI::convertListToText(std::list<ObjectToUpdate> objectsToUpdate)
{
    std::list<std::string> nomsUpdate={} ;

    std::list<std::string> nomsDestroy={} ;

    for (ObjectToUpdate & obj : objectsToUpdate){
        if(obj.updateType==ObjectToUpdate::UpdateType::GEOMETRY){
            nomsUpdate.push_back(obj.name);
            
            obj.convertToText();
        }

        if(obj.updateType==ObjectToUpdate::UpdateType::DESTRUCTION){
            nomsDestroy.push_back(obj.name);
        }
        
    }


    //Push the list of names

    std::ofstream output_file("./Meshes/objListUUID.txt");
    std::ostream_iterator<std::string> output_iterator(output_file, "\n");
    std::copy(nomsUpdate.begin(), nomsUpdate.end(), output_iterator);
    output_file.close();


    std::ofstream output_file0("./Meshes/objListDestroy.txt");
    std::ostream_iterator<std::string> output_iterator0(output_file0, "\n");
    std::copy(nomsDestroy.begin(), nomsDestroy.end(), output_iterator0);
    output_file0.close();
}


