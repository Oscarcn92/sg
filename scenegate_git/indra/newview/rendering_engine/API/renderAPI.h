/** 
 * @file renderAPI.h
 * @brief Description of renderAPI class.
 *
 *
 * 
 */


#include "llviewerobject.h"
#include "llviewerobjectlist.h"
#include "objectToUpdate.h"

class RenderAPI
{
    typedef std::set<LLPointer<LLViewerObject> > vobj_set_t;
private:
    vobj_set_t objCrea;
    vobj_set_t objUpdate ;
    vobj_set_t objUpdateTexture;
    vobj_set_t objKill;
    
    bool tokenUpdateListAvailable = true;

    void convertListToText(std::list<ObjectToUpdate> objectsToUpdate);
    
public:
    // void getObjectsToCreate();
    // void getObjectsToKill();
    // void getObjectsToRebuild();

    RenderAPI();

    void updateUpdateList();
    
    std::list<ObjectToUpdate> getUpdateList();

    struct Update {

        LLViewerObject * object;
        ObjectToUpdate::UpdateType updateType;
    }; 

};
