//Z 2022

#include "apiCameraChanges.hpp"
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <list>

//Main and only instance of the class 
ApiCAMCHANGE livecamchange;


//Constructor
ApiCAMCHANGE::ApiCAMCHANGE(){}




//Stores the current viewercamera information in the current apicam 
void ApiCAMCHANGE::Updateall(LLVector3 Origintosend, LLVector3 vX, LLVector3 vY, LLVector3 vZ){

    setOrigin(Origintosend);
    setX(vX);
    setY(vY);
    setZ(vZ);
    this->Printcam();
}




void ApiCAMCHANGE::Printcam(){
    //LL_WARNS() << "Innit" << LL_ENDL;

    std::ofstream myfile;

    bool canwrite = true;
    std::fstream state;
    std::string a;
    state.open("./camstate.txt");
    while (getline(state, a))
    {
        if (a == "Reading")
        {
            canwrite = false;
        }
    }
    state.close();

    if (canwrite)
    {
        state.open("./camstate.txt", std::ios_base::out |std::ios_base::trunc);
        state << "Writing";
        state.close();
        std::string Origintxt = std::to_string(livecamchange.GetOrigin()[0]) + " \n " + std::to_string(livecamchange.GetOrigin()[VY]) + " \n " + std::to_string(livecamchange.GetOrigin()[VZ]);
        std::string Xtxt = std::to_string(livecamchange.GetX()[VX]) + " \n " + std::to_string(livecamchange.GetX()[VY]) + " \n " + std::to_string(livecamchange.GetX()[VZ]);
        std::string Ytxt = std::to_string(livecamchange.GetY()[VX]) + " \n " + std::to_string(livecamchange.GetY()[VY]) + " \n " + std::to_string(livecamchange.GetY()[VZ]);
        std::string Ztxt = std::to_string(livecamchange.GetZ()[VX]) + " \n " + std::to_string(livecamchange.GetZ()[VY]) + " \n " + std::to_string(livecamchange.GetZ()[VZ]);
        myfile.open("./caminfo.txt");
        myfile << Xtxt << "  \n  " << Ytxt << "  \n  " << Ztxt << "  \n  " << Origintxt;
        myfile.close();
        state.open("./camstate.txt", std::ios_base::out |std::ios_base::trunc);
        state << "Wrote";
        state.close();

    }
    
}


