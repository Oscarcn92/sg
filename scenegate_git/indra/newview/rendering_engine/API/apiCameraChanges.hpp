/*File creating the class ApiCAMCHANGE, A class stocking the current camera.
Will have to be modified to be used with other rendering engines
The current model is simply storing the orgin and the viewpoint of the user (X is look at Z is up and Y is left side)
Be attentive to the changes between rendering engines, for Godot Xg=Xsg but Yg=Zsg and Zg=-Ysg 
*/
//Z 2022
#ifndef _API_CAMCHANGES_H
#define _API_CAMCHANGES_H
#include "llcoordframe.h"




class ApiCAMCHANGE 
{
public:
    LLVector3 Origin;
	LLVector3 X;
	LLVector3 Y;
	LLVector3 Z;


    ApiCAMCHANGE();

    void setOrigin(LLVector3 ori){Origin=ori;};
    void setX(LLVector3 vX){X=vX;};
    void setY(LLVector3 vY){Y=vY;};
    void setZ(LLVector3 vZ){Z=vZ;};
    LLVector3 GetOrigin(){return Origin;};
    LLVector3 GetX(){return X;};
    LLVector3 GetY(){return Y;};
    LLVector3 GetZ(){return Z;};
    void Updateall(LLVector3 Origintosend, LLVector3 vX, LLVector3 vY, LLVector3 vZ); 
    void Printcam();//temporary function printing the camera data in a txt file

private:

};


#endif // _API_CAMCHANGES_H
