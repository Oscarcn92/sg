#include <string>
#include <vector>
#include "llpointer.h"
#include "llviewerobject.h"

class ObjectToUpdate
{
private:
    void setGeometry();
    void setTexture(LLPointer<LLViewerObject> viewerObject);
    

public:

    std::string name;
    LLViewerObject *viewerObject ;
    std::vector<float> position ={0,0,0};
    std::vector<float> scale={0,0,0};
    std::vector<float> rotation={0,0,0};
    float angle =0;

    typedef enum objType
	{
		FLEXIBLE  	= 0,
		ATTACHEMENT 	= 1,
		OTHER = 2,
		
	}ObjType;

    ObjType objType;


    typedef enum updateType
	{
		POSITION  	= 0,
		GEOMETRY 	= 1,
		CREATION		= 2,
		DESTRUCTION		= 3,
		REBUILD		= 4,
        TEXTURE     = 5,
		
	}UpdateType;

    UpdateType updateType;
    
    int nbreFaces = 0;

    struct Face {

        std::vector<float> verticles;
        std::vector<float> normals;
        std::vector<float> indices;
        
        
    };

    struct TextureFace {
        std::vector<u_int8_t> data;
        U16 width;
        U16 height;
        S32 component;
        S32 dataSize;
    };

    std::vector<Face> faces;
    std::vector<TextureFace> TexFaces;
    void translate(std::vector<float> translation);
    void rotate(LLQuaternion quat);
    void convertToText();
    ObjectToUpdate( LLPointer<LLViewerObject> viewerObject, UpdateType _updateType);
};

