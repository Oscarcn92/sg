# README #

This is the main branch of the work done on the Scenegate project in 2021-2022. You can find all the documentation needed inside the **Doc** folder or on the **Documentation** branch.

You can find the original project and compiling instructions on [IMA's bitbucket](https://bitbucket.org/imadevelopmentteam/scenegate-viewer/src/SceneGate/).

