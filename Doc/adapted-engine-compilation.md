

# Adapted Engine Compilation

---

## Understanding Godot's build

The adapted engine needs to compile using Cmake, thus it implements files called **CMakeLists.txt** in order to work properly.
Such files are read when the directory in which they are is added to the project using the **add_subdirectory()** method.

Originally, Godot uses **SCons**, which is a compiler written in **Pyhthon**. Essentially it consists of a file name **SConstruct** which could be compared to the CMakeLists.txt present at the root of our project, and of many **SCsub** files acting as the CMakeLists.txt present in our directories. They are added using **SConscript** method.

To make the engine compile using Cmake, the idea is to understand how Godot's compilation works. Here is a quick introduction :
- SCons being written in Python, it can easily access others scripts that perform actions such as plateform detection or file generation.
System detection is performed early (because it is needed for Environment creation).
- Its main class is **Environment**, this is the class responsible for holding every information about the build. A first Environment object is being built at the root of the project called *env_base*. To this the main options for the build are added and then a new Environment variable called **env** is being created, which will be the one responsible for the main build
- In the SCsub files, the env is being cloned into a new env variable in which can be added specific compile definitions, include paths and link flags before the source code is finally added and compiled.
- In the SCsub files, the Environment variables are used to build libraries later used by the main project. Often, dependencies are added so that when the code added as dependency is updated, the library is rebuilt.

```python
# There are part of the SConstruct code so you can see what have been described above

# System detection
if detect.is_active():
        active_platforms.append(detect.get_name())
        active_platform_ids.append(x)

# Creation of the base Environment variable
env_base = Environment(tools=custom_tools)

# Exemple of adding a compiler definition to the build
if env_base["use_precise_math_checks"]:
    env_base.Append(CPPDEFINES=["PRECISE_MATH_CHECKS"])

# Creation of the env variable, responsible for the main build
if "create" in dir(detect):
    env = detect.create(env_base)
else:
    env = env_base.Clone()
```

```python
# This is some example code from core/SCsub file

# Cloning the main Environment
env_thirdparty = env.Clone()
env_thirdparty.disable_warnings()

# Adding source files
env_thirdparty.add_source_files(thirdparty_obj, thirdparty_misc_sources)

# Adding include path
env_thirdparty.Prepend(CPPPATH=[thirdparty_zstd_dir, thirdparty_zstd_dir + "common"])

# Adding compile definition
env_thirdparty.Append(CPPDEFINES=["ZSTD_STATIC_LINKING_ONLY"])

# Building the new Environment as a library
lib = env.add_library("core", env.core_sources)
env.Prepend(LIBS=[lib])

# Adding dependencies to the library created
env.Depends(lib, thirdparty_obj)

```

See [Godot's documentation about the build system](https://docs.godotengine.org/en/stable/development/compiling/introduction_to_the_buildsystem.html)
and [SCons official website](https://www.scons.org/)

Or even [Godot's documentation on specificly building for Linux](https://docs.godotengine.org/en/stable/development/compiling/compiling_for_x11.html)

---
---

## Compilation using Cmake

The root file for the adapted-engine project is the **CMakeLists.txt** found in the **adapted-engine** folder.
The project is made to be independent : you could just paste the folder into any Cmake project, include the folder and the path, link the created library, and start using it. Here it is added inside the **API** project.

### Project organization :

| Folder      | Content and purpose |
| :---:        |    :----   |
| API      | Where the global API for the rendering engine is located, this is the bridge between Godot and Scenegate |
| adapted-engine   | The root of the adapted engine project, where you can find its api and main files |
| src | The original folder containing Godot, from here everything consists of copying Godot's behavior |
| core/ drivers / main / ... | These are the libraries we want to add to our build, they have their own CMakeLists.txt specifying their behavior|
| other folders under libraries folders | Often a library will have subdirectories where there is just a lot of source files to be compiled in which cases the CMakeLists.txt are quite straight forward |

### Building & Linking

---

We build the whole engine library as a **STATIC** library, meaning it is part of the project. The subdirectories, which are built as libraries when using SCons, are now compiled as **OBJECT**s libraries. Building as object allows us to attach them to the engine and then link only once when linking the main static library.

#### Compile definitions & Link Flags

Almost everything about the behavior of the compilator is copied from Godot's build environment. See wether inside the python code (SConstruct & SCsub) or the summary of the build inside the hidden file **.scons_env.json** that is created when compiling Godot using SCons. A copy of this file is located in **adapted-engine/infos**.

When building Godot the normal way, you can specify options in the command line, but you could also just have a file named **custom.py** inside your Godot folder. SCons will detect it and read all the options specified inside. There is a copy of a custom.py file used for a Godot compilation in the infos folder too.

On the Scenegate side, you can find almost everything about the compilation inside of the indra/cmake directory. Especially the file **00-Common.cmake** is responsible for managing the compile options, definitions and flags to use when compiling Scenegate.

### File generation

---

When compiling with SCons, there are steps where some files are generated. See inside drivers library, there are a lot of generated files for gles.
All the generated files are called filename.**gen**.extension.

```python
# From the SConstruct file
BUILDERS={
    "GLES3_GLSL": env.Builder(
        action=run_in_subprocess(gles_builders.build_gles3_headers), suffix="glsl.gen.h", src_suffix=".glsl"
    )
}
```
The file generation is not yet implemented in the adapted engine, the way it works now is pretty dirty : the \*.gen.\* files has been generated using a normal compilation of Godot and then copied to the project.
There is a way to call python scripts from cmake, we just didn't find time to code this (the dirty solution works fine as we stay on relatively similar computers).

See this [discussion about python call in cmake](https://stackoverflow.com/questions/49053544/how-do-i-run-a-python-script-every-time-in-a-cmake-build) and the [add_custom_command method](https://cmake.org/cmake/help/latest/command/add_custom_command.html)

Or [here in cmake official documentation](https://cmake.org/cmake/help/latest/guide/tutorial/Selecting%20Static%20or%20Shared%20Libraries.html) where they achieve something similar using the same command. (the topic isn't about that but you'll se that they generate a table at compile time, and cmake's documentation is often pretty clear)


### Architecture

---

#### **# The main CMakeLists.txt**

The CMakeLists.txt you can find at the root of the adapted-engine, its role is to create the project, the static library (called **engine**), to set up the globally used options and flags, and at the end to link everything together.

#### **# The CMakeLists.txt inside src directory**

This is the list added by main, it sets a variable specifying how we are going to link Godot libraries (as explained above we chose OBJECT) then sets 2 variables responsible for holding information about how to build the thirdparty library's source files.
Finally it adds every Godot library needed, in a specific order which is important.

The last library added is OS specific. We added platform/x11 without checking anything because we only work with linux but left some clues on how to do that. Trying to make the engine work on any platform would require to check the option added in SCons and do the same in our CMakeLists.txt files.

#### **# The CMakeLists.txt that handle Godot libraries**

These are the lists you find in core, drivers, modules ... directories. They usually start with a call to **add_library()** which creates a new library where we can specify a link mode and some source files. The **target_include_directories()** then allows our source files to see the specified paths when compiling (allows #include "path/file" directives to work properly).

#### **# The CMakeLists.txt inside Godot libraries' subdirectories**

These are often just source files added to the library they are in. Adding source files to an already existing library is done using **target_sources()** method, targeting a library we just created above.


---
---

## Scenegate's requirements

Scenegate uses a library called freetype, a free C library used to render fonts.
See [the official website](https://freetype.org/) for more information.
But Godot uses it too, and has its own internal freetype library.
To avoid any problem, let Scenegate import freetype to the project and comment parts where we import it in the adapted Godot engine.
Note 3 things :
- The commented parts are in **src/scene/CMakeLists.txt** and **src/modules/CMakeLists.txt**. If you were to use the library in another project, you could have to uncomment those to make Godot able to use freetype
- There is also some platform dependent code there, so be careful if you plan on using the adapted-engine's freetype library on another system
- There probably is a better way to do this but at least it works

For now, there is no problem with other libraries.

---
---

## Changes done to Godot

### Godot main executable

---

Normally, Godot compiles whichever main file corresponds to the platform, the main files are named **godot_*platform*.cpp** (or godot_*platform*.mm in the case of apple devices). Those files are the one from which the executable is generated in Godot, thus where you can find the **main(argc, argv)** function.
The class AdaptedEngineApi inside the adapted engine library does the same as godot_x11.cpp's main function, but as an accessible static class.

### Godot 'Main' class

---

Godot's main.cpp and main.h files aren't compiled in the adapted-engine, they have been copied to adapted_engine_main.cpp and adapted_engine_main.hpp so we can have our own Main class if we want to make some changes to it.
We especially changed the **Main::start()** function so that, instead of trying to find a Godot project or to behave differently depending on command line parameters, we directly point to the Mainloop class we want. For now we just run the test_render mainloop to see if we're able to render using the adapted engine.

---
---

## Integration problems and OpenGL

### The actual problem

---

For now, if you try to have both Scenegate and Godot rendering at the same time, the application will crash (and the computer could crash too).
You can see in the **llappviewer.cpp**, inside of frame() function, the parts of the code that have been commented and an early return that have been added.
This way Scenegate only creates the window but does not run any rendering call, and the Godot window can open and run the rendering test properly.

Inside of Godot, the low-level rendering calls are mostly inside of **rasterizer_gles3.cpp** & **rasterizer_scene_gles3.cpp**.
In order to create the window and handle things like input events, Godot uses [GLX](https://en.wikipedia.org/wiki/GLX#/); You can find window and OpenGL context creation in context_gl_x11.cpp.
Similar stuff is handled by [SDL](https://wiki.libsdl.org/FrontPage) in Scenegate's side, and most of the calls can be found in **llviewerdisplay.cpp** & **pipeline.cpp**.
As far as I investigated, Godot was configured to run **GLES3** (which means using OpenGL 3) : 

From Godot's official doc about GLES :
> "GLES2" and "GLES3" are the names used in Godot for the two OpenGL-based rendering backends. In terms of graphics APIs, the GLES2 backend maps to OpenGL 2.1 on desktop, OpenGL ES 2.0 on mobile and WebGL 1.0 on the web. The GLES3 backend maps to OpenGL 3.3 on desktop, OpenGL ES 3.0 on mobile and WebGL 2.0 on the web.

And on its side, Scenegate appears to be using **SDL** (and not SDL2).
The understanding of these 2 libraries will be important if we want to make the application work as intended.

It appears that it's the cohabitation of both GLX and SDL calls that makes the application crash.
It may be something else, but to investigate you'll need a deep understanding of OpenGL.

### Future of the integration

---

Being able to have the 2 sides of the application running and rendering without crashing is only the first part of the work. We will also need to redirect the ouput of Godot's rendering to Scenegate's window. For that we thought about using texture rendering, which means instead of rendering directly to the screen we will render to an OpenGL texture, allowing us to maybe read this texture later in the main thread of Scenegate.
Rendering to a texture is a commonly used technique as it is often used for post processing, so it is well documented.

The thing is that we still need Scenegate to be able to render, mainly because of the user interface, so we can't get rid of SDL.

Here are some usefull links :
- [A tutorial on how to do exactly what we have in mind](http://www.opengl-tutorial.org/fr/intermediate-tutorials/tutorial-14-render-to-texture/), unfortunately it is only in french but ther's still the code and you may find something similar
- [Documentation about GLX library](https://www.khronos.org/opengl/wiki/Programming_OpenGL_in_Linux:_GLX_and_Xlib) which will be usefull if you want to understand how Godot manages to render stuff
- [A tutorial on pixel buffer objects](https://riptutorial.com/opengl/example/28872/using-pbos) describing how to use [pixel buffer objects](https://www.khronos.org/opengl/wiki/Pixel_Buffer_Object) which could be usefull for the future
